var my_JQuery = jQuery.noConflict(true);

function saveApiRest(id,baseUrl) {
	
	
    var form = my_JQuery("#form");
    var loginForm = form.serializeArray();
    var loginFormObject = {};
    var fichero = {};
    my_JQuery.each(loginForm,
    	    function(i, v) {
    			if (v.name==='attachment' || v.name==='fileName' || v.name==='fileExtension' || v.name==='contentType' || v.name==='md5') {
    				fichero[v.name] = v.value;
    			}
    			else {
    				loginFormObject[v.name] = v.value;	
    			}
    	        
    	    });
    loginFormObject["file"]=fichero;
    var jsonData = JSON.stringify(loginFormObject);
    my_JQuery.ajax({
    	type: "POST",
        url: "/rest/gp/integration/1.0/valorations",
        dataType: "json",
        contentType: "application/json",
        async: true, // La petición es asincrona
        cache: false,
        data:jsonData,
        timeout : 10000000,
        error: function(response) {
        	alert(JSON.stringify(response));
        	my_JQuery("#entrada").html(jsonData);
        	my_JQuery("#resultado").html(JSON.stringify(response))
            return false;
        },
        success: function(response){
        	alert(JSON.stringify(response));
        	my_JQuery("#entrada").html(jsonData);
        	my_JQuery("#resultado").html(JSON.stringify(response))
        	return false;
        }
    });
    return false;
}