package com.atsistemas.jira.addons.apimanager.impl;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.apimanager.common.ApiManagerEnum;
import com.atsistemas.jira.addons.apimanager.model.Token;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;


public class ApiManagerServiceImpl implements ApiManagerService {
	private static final Logger logger = LoggerFactory.getLogger(ApiManagerServiceImpl.class);
	
	private final I18nResolver i18nResolver;
	
	
	
//	static {
//	    HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> hostname.equals("77.72.108.78"));
//	}	
	static {
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
	        {
	            public boolean verify(String hostname, SSLSession session)
	            {
	                // ip address of the service URL(like.23.28.244.244)
	                if (hostname.equals("77.72.108.78"))
	                    return true;
	                return false;
	            }
	        });
	}
	
	private static TrustManager[] getTrustManager() {
		return new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
		} };
	}
	
	public ApiManagerServiceImpl(I18nResolver i18nResolver) {
		super();
		this.i18nResolver = i18nResolver;
	}


	public String getTokenHeader() {
		String url = i18nResolver.getText(ApiManagerEnum.URL.value());
		String user = i18nResolver.getText(ApiManagerEnum.USER.value());
		String pass = i18nResolver.getText(ApiManagerEnum.PASS.value());
		
		SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
			// Create a new X509TrustManager
			sslContext.init(null, getTrustManager(), null);
		} catch (Exception e) {
			logger.error("error SSL", e);
			return null;
		}

		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(user, pass));
		WebResource webResource = client.resource(url);

		MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
		formData.add("grant_type", "client_credentials");

		logger.info("Obtenientdo Token apiManager");
		ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class,formData);
		
		if (response.getStatus() != 200) {
			logger.error("Error al obtener token de apiManager");
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}
		String output = response.getEntity(String.class);
		Gson gson = new Gson();
		Token token = gson.fromJson(output, Token.class);
		logger.debug(String.format("Token: %s",token.getAccess_token()));
		return token.getToken_type()+" "+token.getAccess_token();
	}


}
