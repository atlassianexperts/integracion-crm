package com.atsistemas.jira.addons.apimanager.common;

public enum ApiManagerEnum {

	URL(String.valueOf("api-manager-token.url")), USER(String.valueOf("api-manager-token.user")), PASS(
			String.valueOf("api-manager-token.pass"));

	private String value;

	ApiManagerEnum(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public static ApiManagerEnum fromValue(String v) {
		for (ApiManagerEnum b : ApiManagerEnum.values()) {
			if (String.valueOf(b.value).equals(v)) {
				return b;
			}
		}
		return null;
	}

}
