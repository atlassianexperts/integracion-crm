package com.atsistemas.jira.addons.apimanager;

public interface ApiManagerService {
	String getTokenHeader();
}
