package com.atsistemas.jira.addons.jira.workflow;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.jira.workflow.model.StateConfirm;
import com.atsistemas.jira.addons.ufinet.crm.common.CustomFieldsValEco;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import webwork.action.ServletActionContext;

/**
 * This is the post-function class that gets executed at the end of the
 * transition. Any parameters that were saved in your factory class will be
 * available in the transientVars Map.
 */
public class EntregaServicioCliente extends AbstractJiraFunctionProvider {
	private static final Logger logger = LoggerFactory.getLogger(EntregaServicioCliente.class);
	public static final String URL_CONFIRM = "ufinet-jira-crm.url";
	private final ApiManagerService apiManagerService;
	private final I18nResolver i18nResolver;

	public EntregaServicioCliente(ApiManagerService apiManagerService, I18nResolver i18nResolver) {
		super();
		this.apiManagerService = apiManagerService;
		this.i18nResolver = i18nResolver;
	}

	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		MutableIssue issue = getIssue(transientVars);
		HttpServletRequest request = ServletActionContext.getRequest();

		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		CustomField customFieldIdServicioCrm = customFieldManager.getCustomFieldObjectByName("ID servicio CRM");
		String idServicioCRM = (String) issue.getCustomFieldValue(customFieldIdServicioCrm);

		if (idServicioCRM == null || "".equals(idServicioCRM)) {
			return;
		}

		CustomField cfCodigoAdm = customFieldManager.getCustomFieldObjectByName("C�digo administrativo del Servicio");
		CustomField cfFechaEntrega = customFieldManager.getCustomFieldObjectByName("Fecha Entrega");

		String codAdministrativo = (String) issue.getCustomFieldValue(cfCodigoAdm);
		Timestamp fechaEntrega = (Timestamp) issue.getCustomFieldValue(cfFechaEntrega);

		String fechaFormatead = CrmRestUtils.getStringTimestamp(fechaEntrega);
		fechaFormatead += "T00:00:00Z";

		StateConfirm confirm = new StateConfirm(idServicioCRM, "Delivery", fechaFormatead, codAdministrativo);
		Gson gson = new Gson();
		String inputJson = gson.toJson(confirm);
		logger.debug("Json de entrada " + inputJson);

		sendConfirm(i18nResolver.getText(i18nResolver.getText(URL_CONFIRM) + "states"), inputJson);

	}

	private void sendConfirm(String url, String input) {
		String headerToken = apiManagerService.getTokenHeader();

		Client client = Client.create();
		WebResource webResource = client.resource(url);

		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("Authorization", headerToken)
				.post(ClientResponse.class, input);
		if (response.getStatus() != 200) {
			String outPutJson = response.getEntity(String.class);
			logger.error("Error en la respuesta Delivery CRM " + response.getStatus() + " " + outPutJson);
		}

	}
}