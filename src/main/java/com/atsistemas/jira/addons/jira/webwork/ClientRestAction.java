package com.atsistemas.jira.addons.jira.webwork;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Cause;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectsClient;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectsHome;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;
import com.atsistemas.jira.addons.ufinet.crm.service.SearchIssueService;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class ClientRestAction extends JiraWebActionSupport {
	
	private static final Logger logger = LoggerFactory.getLogger(ClientRestAction.class);
	
	private static final String[] DEPARTAMENTOS = { "Ingenier�a Colombia", "Ingenier�a Costa Rica", "Ingenier�a Panam�",
			"Ingenier�a Ecuador", "Ingenier�a Paraguay", "Ingenier�a El Salvador", "Ingenier�a Honduras",
			"Ingenier�a Nicaragua", "Ingenier�a Guatemala", "Ingenier�a Clientes Espa�a", "Ingenier�a Clientes Latam" };
	
	private static final String URL_PROJECT = "/rest/gp/integration/project/1.0/projects";
	private static final String URL_SERVICES = "/rest/gp/integration/project/1.0/services";
	
	private final ProjectsService createProjectService;
	private final CreateServService createServService;
	private final SearchIssueService searchIssueService;
	private final JiraAuthenticationContext jiraAuthenticationContext;
	
	private String inputJson;
	private String outPutJson;
	
	public ClientRestAction(ProjectsService createProjectService, CreateServService createServService,SearchIssueService searchIssueService,JiraAuthenticationContext jiraAuthenticationContext) {
		super();
		this.createProjectService = createProjectService;
		this.createServService = createServService;
		this.searchIssueService=searchIssueService;
		this.jiraAuthenticationContext=jiraAuthenticationContext;
	}

	@Override
	protected String doExecute() throws Exception {

		return doSendRest();
	}

	public String doSendRest() throws Exception {

		logger.debug("INICIO");
		Gson gson = new Gson();
		inputJson = gson.toJson(getProjectSetUp());
		
		
//		logger.debug(inputJson);
		String fullUrl = this.getHttpRequest().getRequestURL().toString();
		String uri = this.getHttpRequest().getRequestURI();
		String requestPath = fullUrl.substring(0, fullUrl.indexOf(uri));

		try {
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter("fQRw_REyBPu9XfM3YXpHYT0WSnIa", "K4AVRLzhm_o4EfyifqH1M1JIa8Ea"));
			
			//WebResource webResource = client.resource(requestPath + URL_PROJECT);
			WebResource webResource = client.resource("https://apimanager.ufinet.com/token");
			
			ClientResponse response = webResource.post(ClientResponse.class);
			outPutJson = response.getEntity(String.class);
			
			logger.debug(String.format("Salida rest: %s", outPutJson));
			if (response.getStatus() != 200) {
				logger.error(String.format("Codigo error: %s",Integer.toString(response.getStatus())));
				
			}
			
			
			this.getHttpRequest().setAttribute("json", outPutJson);

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS; // returns SUCCESS
	}

	public String doSendServiceStates() throws Exception {

		Cause cause = new Cause();
		cause.setCause("causa");
		cause.setCode("code");
		cause.setDate("01/01/2017");
		
		
		logger.debug("INICIO");
		Gson gson = new Gson();
		inputJson = gson.toJson(cause);
		
		
		logger.debug(inputJson);
		String fullUrl = this.getHttpRequest().getRequestURL().toString();
		String uri = this.getHttpRequest().getRequestURI();
		String requestPath = fullUrl.substring(0, fullUrl.indexOf(uri));

		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
		WebResource webResource = client.resource(requestPath + URL_SERVICES+"/idServicio/cancel");

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, inputJson);
		outPutJson = response.getEntity(String.class);
		
		logger.debug(String.format("Salida rest: %s", outPutJson));
		if (response.getStatus() != 200) {
			logger.error(String.format("Codigo error: %s",Integer.toString(response.getStatus())));
			
		}
		
		return SUCCESS; // returns SUCCESS
	}
	
	
	public String doSend() throws Exception {
//		ProjectServicesRest servicioRest = new ProjectServicesRest(createProjectService, createServService,jiraAuthenticationContext);
//		Gson gson = new Gson();
//		
//		logger.debug(gson.toJson(getProjectSetUp()));
//		
//		Response respuesta = servicioRest.getMessage(getProjectSetUp());
//		respuesta.getEntity().toString();

		return SUCCESS;
	}

	public String doListado() {
		
		List<Issue> listado;
		try {
			listado = searchIssueService.searchIssueFromCustomField("GPRO", "Proyecto", "ID Proyecto CRM", "idCrm",CrmRestUtils.getUserCrm(null));
			for (Issue item : listado) {
				
				logger.debug("item "+ item.getKey());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return SUCCESS;

	}
	

	
	private ProjectServicesCrm getProjectSetUp() {
		
		ProjectServicesCrm project = new ProjectServicesCrm();
		project.setClient("cliente");
		project.setCountry("pais");
		project.setDescription("descripcion");
		project.setFinalClient("cliente final");
		project.setIdProject("idCrm");
		project.setImputationCode("codImputacion");
		project.setInformer("informador");
		project.setName("nombre del proyecto CRM");
		project.setValEcoCode("val economica");
		project.setAssignedDepartment("Ingenier�a Colombia");
		project.setServices(getServiceListSetUp());

		return project;
	}

	private ProjectsHome getProjectHome() {
		ProjectsHome home = new ProjectsHome();
		home.setAddress("direccion");
		home.setCountry("country");
		home.setLatitude("latitude");
		home.setLongitude("longitude");
		home.setRegion("region");
		home.setSite("site");
		home.setTown("town");
		
		return home;
	}
	
	private ProjectsClient getCient() {
		ProjectsClient client = new ProjectsClient();
		
		client.setContactEmail("email");
		client.setContactName("name");
		client.setContactNumber("number");
		return client;
	}
	
	private List<Service> getServiceListSetUp() {
		ArrayList<Service> listado = new ArrayList<>();
		for (String departamento : DEPARTAMENTOS) {
			for (int i = 0; i < 1; i++) {
				Service servicio = new Service();
				servicio.setAssignedDepartment("Ingenier�a Colombia");
				servicio.setCapability("capacidad");
				servicio.setCode("codigo");
				servicio.setComment("comment");
				
				servicio.setHome(getProjectHome());
				servicio.setClient(getCient());
				servicio.setDestination(getProjectHome());
				servicio.setDescription("desripcion");
				
				servicio.setFibersNumber("1");
				servicio.setIdService("idServicio");
				servicio.setTransmission("transmision");
				servicio.setServiceBudget("presupuesto");
				servicio.setComment("observicaciones");
				listado.add(servicio);
			}

		}
		return listado;
	}

//	private ProjectServicesCrm getProjectServices() {
//		ProjectServicesCrm projectServices = new ProjectServicesCrm();
//		projectServices.setProyecto(getProjectSetUp());
//		projectServices.setService(getServiceListSetUp());
//		return projectServices;
//
//	}

	public String getInputJson() {
		return inputJson;
	}

	public void setInputJson(String inputJson) {
		this.inputJson = inputJson;
	}

	public String getOutPutJson() {
		return outPutJson;
	}

	public void setOutPutJson(String outPutJson) {
		this.outPutJson = outPutJson;
	}

}
