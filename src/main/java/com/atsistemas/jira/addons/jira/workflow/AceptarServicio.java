package com.atsistemas.jira.addons.jira.workflow;

import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.jira.workflow.model.StateConfirm;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import webwork.action.ServletActionContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the post-function class that gets executed at the end of the
 * transition. Any parameters that were saved in your factory class will be
 * available in the transientVars Map.
 */
public class AceptarServicio extends AbstractJiraFunctionProvider {
	private static final Logger logger = LoggerFactory.getLogger(AceptarServicio.class);
	public static final String FIELD_MESSAGE = "messageField";
	private final ApiManagerService apiManagerService;
	private final I18nResolver i18nResolver;
	public static final String URL_CONFIRM = "ufinet-jira-crm.url";

	public AceptarServicio(ApiManagerService apiManagerService, I18nResolver i18nResolver) {
		super();
		this.apiManagerService = apiManagerService;
		this.i18nResolver = i18nResolver;
	}

	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
		MutableIssue issue = getIssue(transientVars);

		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

		CustomField customFieldIdServicioCrm = customFieldManager.getCustomFieldObjectByName("ID servicio CRM");

		String idServicioCRM = (String) issue.getCustomFieldValue(customFieldIdServicioCrm);

		if (idServicioCRM == null || "".equals(idServicioCRM)) {
			return;
		}

		CustomField cfFechaEntrega = customFieldManager.getCustomFieldObjectByName("Fecha de Aceptaci�n del Servicio");

		Timestamp fechaEntrega = (Timestamp) issue.getCustomFieldValue(cfFechaEntrega);

		String fechaFormatead = CrmRestUtils.getStringTimestamp(fechaEntrega);
		fechaFormatead += "T00:00:00Z";

		CustomField customFieldCodAm = customFieldManager
				.getCustomFieldObjectByName("C�digo administrativo del Servicio");
		String codAdm = (String) issue.getCustomFieldValue(customFieldCodAm);

		StateConfirm confirm = new StateConfirm(idServicioCRM, "Accepted", fechaFormatead, codAdm);
		Gson gson = new Gson();
		String inputJson = gson.toJson(confirm);

		sendConfirm(i18nResolver.getText(i18nResolver.getText(URL_CONFIRM) + "states"), inputJson);

	}

	private void sendConfirm(String url, String input) {
		String headerToken = apiManagerService.getTokenHeader();

		Client client = Client.create();
		WebResource webResource = client.resource(url);

		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("Authorization", headerToken)
				.post(ClientResponse.class, input);
		if (response.getStatus() != 200) {
			String outPutJson = response.getEntity(String.class);
			logger.error("Error en la respuesta Delivery CRM " + response.getStatus() + " " + outPutJson);
		}

	}
}