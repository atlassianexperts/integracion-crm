package com.atsistemas.jira.addons.jira.workflow.model;

public class StateConfirm {

	private String id;
	private String status;
	private String date;
	private String adminCode;
	
	
	public StateConfirm(String id, String status, String dateSates, String adminCode) {
		super();
		this.id = id;
		this.status = status;
		this.date = dateSates;
		this.adminCode = adminCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateSates() {
		return date;
	}
	public void setDateSates(String dateSates) {
		this.date = dateSates;
	}
	public String getAdminCode() {
		return adminCode;
	}
	public void setAdminCode(String adminCode) {
		this.adminCode = adminCode;
	}
	
	
	
	
}
