package com.atsistemas.jira.addons.jira.webwork;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atsistemas.jira.addons.ufinet.crm.rest.RestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.FileInputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;
import com.google.gson.Gson;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class FormCrmAction extends JiraWebActionSupport {
	private static String URL = "/rest/gp/integration/1.0/services";

	private static final Logger log = LoggerFactory.getLogger(FormCrmAction.class);

	@Override
	public String execute() throws Exception {

		return super.execute(); // returns SUCCESS
	}

	@Override
	protected String doExecute() throws Exception {

		return INPUT;
	}

	public String doSendRest() {
		InputRestCrmValEco input = new InputRestCrmValEco();
		input.setId(getHttpRequest().getParameter("id"));
		input.setNameApplication(getHttpRequest().getParameter("nameApplication"));

		input.setSummary(getHttpRequest().getParameter("summary"));

		input.setDescription(getHttpRequest().getParameter("description"));
		input.setClient(getHttpRequest().getParameter("client"));

		input.setFinalClient(getHttpRequest().getParameter("finalClient"));
		input.setOfferNumber(getHttpRequest().getParameter("offerNumber"));

		input.setTargetDate(getHttpRequest().getParameter("targetDate"));
		input.setAssignedDepartment(getHttpRequest().getParameter("assignedDepartment"));
		input.setScope(getHttpRequest().getParameter("scope"));

		FileInputRestCrmValEco fichero = new FileInputRestCrmValEco();

		fichero.setAttachment(getHttpRequest().getParameter("attachment"));
		fichero.setFileName(getHttpRequest().getParameter("fileName"));
		fichero.setFileExtension(getHttpRequest().getParameter("fileExtension"));
		fichero.setContentType(getHttpRequest().getParameter("contentType"));

		input.setFile(fichero);

		Gson gson = new Gson();
		String data = gson.toJson(input);
		

		return sendAsyncJson(data);
		
	}
	
	public String doJson() {
		return "inputjson";
	}
	
	public String doJSend() {
		
		String sJson = this.getHttpRequest().getParameter("json");
		
		return sendJson(sJson);
	}
	
	private String sendAsyncJson(String sJson) {
		String fullUrl = this.getHttpRequest().getRequestURL().toString();
		String uri = this.getHttpRequest().getRequestURI().toString();
		String requestPath = fullUrl.substring(0, fullUrl.indexOf(this.getHttpRequest().getRequestURI().toString()));

		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
		AsyncWebResource  webResource = client.asyncResource(requestPath + URL);
		
		Future<ClientResponse> response = webResource.type("application/json").post(ClientResponse.class, sJson);


		return SUCCESS;
		
	}
	
	private String sendJson(String sJson) {
		
		
		String fullUrl = this.getHttpRequest().getRequestURL().toString();
		String uri = this.getHttpRequest().getRequestURI().toString();
		String requestPath = fullUrl.substring(0, fullUrl.indexOf(this.getHttpRequest().getRequestURI().toString()));

		Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
		WebResource webResource = client.resource(requestPath + URL);

		ClientResponse response = webResource.type("application/json").delete(ClientResponse.class, sJson);

		// ClientResponse response =
		// webResource.accept("application/json").post(ClientResponse.class,
		// data);

		if (response.getStatus() != 202) {
			log.error("Codigo error:" + Integer.toString(response.getStatus()));
			return ERROR;
		} else {
			log.debug("Salida rest:" + webResource.get(String.class));
		}
		return SUCCESS;
	}
}
