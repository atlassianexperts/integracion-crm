package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.common.ProjectCustomFieldName;
import com.atsistemas.jira.addons.ufinet.crm.service.common.PropertiesName;
import com.atsistemas.jira.addons.ufinet.crm.service.utils.CustomFieldUtils;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.atsistemas.jira.addons.ufinet.utils.UfinetUtils;


public class CreateServServiceMutableImpl implements CreateServService {

	private final ProjectManager projectManager;
	private final I18nResolver i18nResolver;
	private final IssueFactory issueFactory;
	private final IssueManager issueManager;

	private static final Logger logger = LoggerFactory.getLogger(CustomFieldUtils.class);

	public CreateServServiceMutableImpl(ProjectManager projectManager, I18nResolver i18nResolver,
			IssueFactory issueFactory, IssueManager issueManager) {
		super();
		this.projectManager = projectManager;
		this.i18nResolver = i18nResolver;
		this.issueFactory = issueFactory;
		this.issueManager = issueManager;
	}

	@Override
	public Issue createService(Service servicioCrm, Issue proyecto, String tipoOperacion) throws CreateException {

		try {
			MutableIssue newIssue = createMutableIssueService(servicioCrm,tipoOperacion,proyecto);
			
			return issueManager.createIssueObject(CrmRestUtils.getUserCrm(servicioCrm.getInformer()), newIssue);
		} catch (Exception e) {
			logger.error("-Create issue", e);
			throw new CreateException("Create issue", e);
		}

	}

	private MutableIssue createMutableIssueService(Service servicioCrm, String tipoOperacion,Issue proyecto) throws Exception {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		Project project = projectManager.getProjectObjByKey(i18nResolver.getText(PropertiesName.SERVICE_PROJECT_NAME));
		IssueType servicioIssueType = UfinetUtils.getIssueType(project,
				i18nResolver.getText(PropertiesName.SERVICE_ISSUETYPE_NAME));

		MutableIssue newIssue = issueFactory.getIssue();
		newIssue.setProjectObject(project);
//		
		newIssue.setSummary(servicioCrm.getName());
		newIssue.setDescription(servicioCrm.getDescription());
		newIssue.setIssueTypeObject(servicioIssueType);
		logger.trace("Reporter "+CrmRestUtils.getUserCrm(servicioCrm.getInformer()));
		newIssue.setReporter(CrmRestUtils.getUserCrm(servicioCrm.getInformer()));

		// Area/Departamento Asignado
		CustomField area = UfinetUtils.getCustomfieldSelected(project.getId(), servicioIssueType.getId(),
				i18nResolver.getText(ProjectCustomFieldName.DEPARTAMENTO_ASIGNADO));
		
		logger.debug(""+area);
		
		Option opcionSel = UfinetUtils.getOptionFormCustomField(area,
				"Default Configuration Scheme for Area/Departamento Asignado", servicioCrm.getAssignedDepartment());
		logger.trace("area sel " + area);
		newIssue.setCustomFieldValue(area, opcionSel);

		
		CustomField cfGrupo = customFieldManager.getCustomFieldObjectByName("Grupo");
		newIssue.setCustomFieldValue(cfGrupo, proyecto.getCustomFieldValue(cfGrupo));

		
		// Tipo de servicio Jira
		CustomField tipoServicio = customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipos de servicio JIRA");
		Option opcionTipoSel = UfinetUtils.getOptionFormCustomField(tipoServicio,
				"Default Configuration Scheme for MAAT GSER - Tipos de servicio JIRA", servicioCrm.getAssignedDepartment());
		
		logger.trace("tipo de servicio  " + opcionTipoSel);
		newIssue.setCustomFieldValue(tipoServicio, opcionTipoSel);

		// Prepusuesto
		CustomField presupuesto = UfinetUtils.getCustomfieldSelected(project.getId(), servicioIssueType.getId(),
				i18nResolver.getText(PropertiesName.PRESUPUESTO));
		Double lPresupuesto = new Double(servicioCrm.getServiceBudget());
		newIssue.setCustomFieldValue(presupuesto, lPresupuesto);

		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("ID servicio CRM"),
				servicioCrm.getIdService());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Cliente Servicio"),
				servicioCrm.getClientName());

		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Fecha de Compromiso Servicio"),
				CrmRestUtils.getTargetDateTimestamp(servicioCrm.getTargetDate()));
		newIssue.setCustomFieldValue(
				customFieldManager.getCustomFieldObjectByName("C�digo administrativo del Servicio"),
				servicioCrm.getCode());

		logger.trace("Datos del contacto");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Nombre del Contacto"),
				servicioCrm.getClient().getContactName());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Tel�fono del Contacto"),
				servicioCrm.getClient().getContactNumber());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Email del Contacto"),
				servicioCrm.getClient().getContactEmail());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("ID Cliente CRM"),
				servicioCrm.getClient().getIdClient());
				
		

		logger.trace("PI");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("PI"), servicioCrm.getPi());

		logger.trace("Sitio origen");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Sitio Origen"),
				servicioCrm.getHome().getSite());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Direcci�n origen"),
				servicioCrm.getHome().getAddress());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Latitud origen"),
				servicioCrm.getHome().getLatitude());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Longitud origen"),
				servicioCrm.getHome().getLongitude());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Provincia origen"),
				servicioCrm.getHome().getRegion());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Municipio origen"),
				servicioCrm.getHome().getTown());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Pa�s origen"),
				servicioCrm.getHome().getCountry());

		logger.trace("Sitio destino");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Sitio Destino"),
				servicioCrm.getDestination().getSite());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Direcci�n destino"),
				servicioCrm.getDestination().getAddress());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Latitud destino"),
				servicioCrm.getDestination().getLatitude());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Longitud destino"),
				servicioCrm.getDestination().getLongitude());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Provincia destino"),
				servicioCrm.getDestination().getRegion());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Municipio destino"),
				servicioCrm.getDestination().getTown());
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Pa�s destino"),
				servicioCrm.getDestination().getCountry());

		logger.trace("Medio");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Medio de transmisi�n"),
				servicioCrm.getTransmission());

		logger.trace("Capacidad");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Capacidad Servicio"),
				servicioCrm.getCapability());

		logger.trace("Velocidad");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Velocidad / N� Fibras"),
				servicioCrm.getFibersNumber());

		logger.trace("Id valoracion");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("ID Valoraci�n Econ�mica"),
				servicioCrm.getIdValEco());

		logger.trace("Observaciones");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Observaciones"),
				servicioCrm.getComment());

		logger.trace("Tipo operacion");
		newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Tipo Operaci�n CRM"),
				tipoOperacion);
		
		return newIssue;
	}


}
