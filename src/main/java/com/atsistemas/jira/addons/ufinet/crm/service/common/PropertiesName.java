package com.atsistemas.jira.addons.ufinet.crm.service.common;

public class PropertiesName {
	
	public static final String PROJECT_NAME="proyectos-servicio-rest.proyectos.key";
	public static final String PROJECT_ISSUETYPE_NAME="proyectos-servicio-rest.proyecto.issueType";
	public static final String PROJECT_ISSUELINKTYPE_NAME="proyectos-servicio-rest.proyecto.linkTypeName";
	public static final String PROJECT_CUSTOMFIELD_NAME="proyectos-servicio-rest.proyecto.customField.idProyectoCRM";
	
	public static final String SERVICE_PROJECT_NAME="proyectos-servicio-rest.servicio.key";
	public static final String SERVICE_ISSUETYPE_NAME="proyectos-servicio-rest.servicio.issueType";
	public static final String SERVICE_ISSUELINKTYPE_NAME="proyectos-servicio-rest.proyecto.linkTypeName";
	public static final String CUSTOM_FIELD_ID_SERVICIO_CRM="proyectos-servicio-rest.servicio.idServicioCrm";
	public static final String CUSTOM_FIELD_TITULO_CORTO="proyectos-servicio-rest.servicio.titulo_corto";
	public static final String CUSTOM_FIELD_CLIENTE_SERVICIO="proyectos-servicio-rest.servicio.clienteServicio";
	public static final String TITULO_CORTO = "proyectos-servicio-rest.servicio.titulo_corto";
	public static final String FECHA_COMPROMISO = "proyectos-servicio-rest.servicio.fecha_compromiso";
	public static final String TIPO_SERVICIO = "proyectos-servicio-rest.servicio.tipo_servicio";
	public static final String TIPO_CODIGO_ADMINISTRATIVO = "proyectos-servicio-rest.servicio.tipo_codigo_administrativo";
	public static final String NOMBRE_CONTACTO = "proyectos-servicio-rest.servicio.nombre_contacto";
	public static final String TELEFONO_CONTACTO = "proyectos-servicio-rest.servicio.telefono_contacto";
	public static final String EMAIL_CONTACTO = "proyectos-servicio-rest.servicio.email_contacto";
	public static final String PI = "proyectos-servicio-rest.servicio.pi";
	public static final String SITIO_ORIGEN = "proyectos-servicio-rest.servicio.sitio_origen";
	public static final String DIRECCION_ORIGEN = "proyectos-servicio-rest.servicio.direccion_origen";
	public static final String LATITUD_ORIGEN = "proyectos-servicio-rest.servicio.latitud_origen";
	public static final String LONGITUD_ORIGEN = "proyectos-servicio-rest.servicio.longitud_origen";
	public static final String LOCATION = "proyectos-servicio-rest.servicio.location";
	public static final String PROVINCIA_ORIGEN = "proyectos-servicio-rest.servicio.provincia_origen";
	public static final String MUNICIPIO_ORIGEN = "proyectos-servicio-rest.servicio.municipio_origen";
	public static final String PAIS_ORIGEN = "proyectos-servicio-rest.servicio.pais_origen";
	public static final String SITIO_DESTINO = "proyectos-servicio-rest.servicio.sitio_destino";
	public static final String DIRECCION_DESTINO = "proyectos-servicio-rest.servicio.direccion_destino";
	public static final String LATITUD_DESTINO = "proyectos-servicio-rest.servicio.latitud_destino";
	public static final String LONGITUD_DESTINO = "proyectos-servicio-rest.servicio.longitud_destino";
	public static final String PROVINCIA_DESTINO = "proyectos-servicio-rest.servicio.provincia_destino";
	public static final String MUNICIPIO_DESTINO = "proyectos-servicio-rest.servicio.municipio_destino";
	public static final String PAIS_DESTINO = "proyectos-servicio-rest.servicio.pais_destino";
	public static final String TRANSMISION = "proyectos-servicio-rest.servicio.transmision";
	public static final String CAPACIDAD = "proyectos-servicio-rest.servicio.capacidad";
	public static final String FIBRAS = "proyectos-servicio-rest.servicio.fibras";
	public static final String ID_VALORACION_ECO = "proyectos-servicio-rest.servicio.id_valoracion_eco";
	public static final String PRESUPUESTO = "proyectos-servicio-rest.servicio.presupuesto";
	public static final String OBSERVACIONES = "proyectos-servicio-rest.servicio.observaciones";
	
	private PropertiesName() {
	}

}
