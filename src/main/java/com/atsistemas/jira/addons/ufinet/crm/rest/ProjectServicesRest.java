package com.atsistemas.jira.addons.ufinet.crm.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.DetailErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.ErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.CancelProject;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;
import com.atsistemas.jira.addons.ufinet.crm.service.TransitionService;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;

/**
 * A resource of message.
 */
@Path("/projects")
public class ProjectServicesRest {
	private static final Logger log = LoggerFactory.getLogger(ProjectServicesRest.class);
	private final ProjectsService createProjectService;
	private final CreateServService createServService;
	private final TransitionService transitionService;


	public ProjectServicesRest(ProjectsService createProjectService, CreateServService createServService,
			TransitionService transitionService) {
		super();
		this.createProjectService = createProjectService;
		this.createServService = createServService;
		this.transitionService = transitionService;
	}

	@POST
	@Path("/")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response getAltaProyectoServicios(ProjectServicesCrm projectServices) {
		
		Gson gson = new Gson();
		String inputJson = gson.toJson(projectServices);
		log.debug("INPUT:"+inputJson);
		
		User informerProject = CrmRestUtils.getUserCrm(projectServices.getInformer());
		
		if (informerProject==null) {
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
					Response.Status.BAD_REQUEST.getReasonPhrase(), "Informador no encontrado");

			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();				
		}
		
		log.trace("--- Inicio thread ----");
		Thread hilo = new Thread(new CreateProjectServicesThread(projectServices,createProjectService,createServService));
		hilo.start();

		
		log.trace("--- enviando respuesta ----");
		return Response.ok("Solicitud aceptada.").build();
	}

	@PUT
	@Path("/")
	@Consumes(APPLICATION_JSON)
	@Produces(APPLICATION_JSON)
	public Response getCancelProject(CancelProject cancelProject) {
		Gson gson = new Gson();
		String inputJson = gson.toJson(cancelProject);
		log.debug("INPUT CANCELACION:"+inputJson);

		
		try {
			transitionService.transitionCancelProject(cancelProject);
			return Response.ok("").build();
		} catch (Exception e) {
			log.error("error creating issue", e);
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco("500",
					Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.toString());
			return Response.serverError().entity(new ErrorCrmValEco(detail)).build();		
		}
	}

}