package com.atsistemas.jira.addons.ufinet.crm.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ModificationTypes {
	private ModificationTypes() {
		
	}
    public static final Map<String, String> NO_CHANGE_TECNOLOGY;
    static {
    	Map<String, String> aMap = new HashMap<>();
    	aMap.put("1","AUMENTO CAPACIDAD");
    	aMap.put("2","DISMINUIR CAPACIDAD");
    	aMap.put("3","CAMBIO ORIGEN Y/O DESTINO");
    	aMap.put("5", "AUMENTO CAPACIDAD Y CAMBIO DE ORIGEN Y/O DESTINO");
    	aMap.put("7","DISMINUIR CAPACIDAD Y CAMBIO SITIO ORIGEN Y DESTINO");
        NO_CHANGE_TECNOLOGY = Collections.unmodifiableMap(aMap);
    }

    public static final Map<String, String> CHANGE_TECNOLOGY;
    static {
    	Map<String, String> aMap = new HashMap<>();
    	aMap.put("4","Cambio de tecnología");
    	aMap.put("6","Aumento capacidad y cambio de tecnología");
    	aMap.put("8","Disminuir capacidad y cambio de tecnología");
    	aMap.put("9","Cambio sitio origen y cambio de tecnología");
    	aMap.put("10","Aumento de capacidad y cambio de sitio y origen, mas cambio de tecnología");
    	aMap.put("11","Diminuir capacidad y cambio de sitio origen y destino, mas cambio de tecnología");
        CHANGE_TECNOLOGY = Collections.unmodifiableMap(aMap);
    }

}
