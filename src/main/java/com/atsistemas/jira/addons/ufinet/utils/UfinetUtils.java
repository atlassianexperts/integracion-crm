package com.atsistemas.jira.addons.ufinet.utils;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;

public class UfinetUtils {
	private static final Logger log = LoggerFactory.getLogger(UfinetUtils.class);
	
	
	private UfinetUtils() {
		
	}

	public static IssueType getIssueType(Project proyect, String issueTypeName) {
		for (IssueType item : proyect.getIssueTypes()) {
			log.debug("Issue Type:" + item.getName());
			if (issueTypeName.equals(item.getName())) {
				return item;
			}
		}
		return null;

	}

	public static CustomField getCustomfieldSelected(Long idProject, String issueType, String customFieldSel) throws Exception {
		CustomField customSel = null;
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		customSel = customFieldManager.getCustomFieldObjectByName(customFieldSel);
		
		return customSel;
	}

	private static FieldConfig getFieldConfig(CustomField customField, String schemSel) {

		for (FieldConfigScheme itemFieldConfig : customField.getConfigurationSchemes()) {
			log.trace("Esquema:" + itemFieldConfig.getName());

			if (itemFieldConfig.getName().equals(schemSel)) {
				return itemFieldConfig.getOneAndOnlyConfig();
			}
		}
		return null;

	}

	public static Option getOptionFormCustomField(CustomField customField, String scheme, String optionSel) {
		String optionUtf8 = null;
		FieldConfig schemeSel = getFieldConfig(customField, scheme);

		Options options = ComponentAccessor.getOptionsManager().getOptions(schemeSel);
		
		
		Option optionFound = null;

		for (Option o : options.getRootOptions()) {
			try {

				optionUtf8 = new String(o.getValue().getBytes("UTF-8"));
				log.debug(String.format("Option-> %s", optionUtf8));
				if (optionUtf8.equalsIgnoreCase(optionSel) || (o.getValue().equals(optionSel))) {
					log.debug(String.format("Option found: %s %s",optionUtf8,optionSel));
					optionFound = o;
				}
			} catch (UnsupportedEncodingException e) {
				log.error("", e);
			}

		}
		
		 
		return optionFound;
	}



}
