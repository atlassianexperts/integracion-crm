package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.web.util.AttachmentException;
import com.atsistemas.jira.addons.ufinet.crm.common.CustomFieldsValEco;
import com.atsistemas.jira.addons.ufinet.crm.dto.AttachmentDTO;
import com.atsistemas.jira.addons.ufinet.crm.exception.RestCrmException;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.service.IssueCrmService;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;

public class IssueCrmServiceImpl implements IssueCrmService {

	private static final Logger log = LoggerFactory.getLogger(IssueCrmServiceImpl.class);
	private String userJira;
	private String projectName;
	private InputRestCrmValEco model;
	private PropertiesConfiguration config = null;
	private static final String CONFIG_FILE = "ufinet-jira-crm.properties";
	private static final String ISSUE_TYPE = "Valoraci�n econ�mica";

	public IssueCrmServiceImpl(InputRestCrmValEco model) throws RestCrmException {
		super();

		File f = null;
		if (System.getProperty("ufinet-jira-crm.property") != null)
			f = new File(System.getProperty("ufinet-jira-crm.property"));
		try {

			if (f != null && f.exists()) {
				log.info("Using Configuration " + f.getAbsolutePath());

				config = new PropertiesConfiguration(f);

			} else {
				f = new File(new File("."), CONFIG_FILE);
				if (f.exists()) {
					log.info("Using Configuration " + f.getAbsolutePath());
					config = new PropertiesConfiguration(f);
				} else {
					log.info("Using default Configuration");
					config = new PropertiesConfiguration(CONFIG_FILE);
				}
			}
			projectName = config.getString("ufinet-jira-crm.project");

			this.userJira = config.getString("ufinet-jira-crm.user");
			this.model = model;
		} catch (ConfigurationException e) {
			log.error("", e);
			throw new RestCrmException("Create issue", e);
		}
	}

	public IssueCrmServiceImpl(String projectName, InputRestCrmValEco model) {
		super();
		this.projectName = projectName;
		this.model = model;
	}

	//
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public InputRestCrmValEco getModel() {
		return model;
	}

	public void setModel(InputRestCrmValEco model) {
		this.model = model;
	}

	public Issue createIssue() throws RestCrmException {

		return createIssue(projectName, ISSUE_TYPE, model);
	}

	public Issue createIssue(String projectName, String issueTypeName, InputRestCrmValEco model)
			throws RestCrmException {

		IssueManager issueManager = ComponentAccessor.getIssueManager();


		try {
			User user = CrmRestUtils.getUserCrm(model.getInformer());
			if (user == null) {
				throw new RestCrmException(String.format("Informador no encontrado %s", model.getInformer()));
			}
			
			MutableIssue newIssue = createMutableIssue();
			newIssue.setReporter(user);
			return issueManager.createIssueObject(user, newIssue);
		} catch (Exception e) {
			log.error("Create issue", e);
			throw new RestCrmException("Create issue", e);
		}
	}

	private MutableIssue createMutableIssue() throws CreateException {
		IssueFactory issueFactory = ComponentAccessor.getIssueFactory();
		Project proyect = ComponentAccessor.getProjectManager().getProjectObjByKey(projectName);

		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

		Timestamp targetDate = getTargetDate(model.getTargetDate());

		CustomField assignedDepartment = customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.ASSIGNED_DEPARTMENT);
		if (assignedDepartment == null) {
			assignedDepartment = CrmRestUtils.getCustomfieldSelected(proyect.getId(), ISSUE_TYPE,
					CustomFieldsValEco.ASSIGNED_DEPARTMENT);
		}
		log.debug(String.format("CustomField departamento Asignado selected: %s  option selected %s",
				assignedDepartment, model.getAssignedDepartment()));

		Option optionSelected = CrmRestUtils.getOptionFormCustomField(assignedDepartment,
				CustomFieldsValEco.FIELD_SCHEME, model.getAssignedDepartment());

		log.info(String.format("AssignedDepartment selected: %s", optionSelected));

		CustomField scope = customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.SCOPE);

		if (scope == null) {
			scope = CrmRestUtils.getCustomfieldSelected(proyect.getId(), ISSUE_TYPE, CustomFieldsValEco.SCOPE);
		}

		log.debug(String.format("CustomField  ambito selected: %s  option selected %s", scope, model.getScope()));

		Option scopeSelected = CrmRestUtils.getOptionFormCustomField(scope, CustomFieldsValEco.FIELD_SCHEME_AMBITO,
				model.getScope());
		log.info(String.format("Ambito selected: %s", scopeSelected));

		CustomField client = customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.CLIENT);
		
		
		IssueType itemSel = getIssueType(projectName, ISSUE_TYPE);
		if (itemSel == null) {
			throw new CreateException("issue type not found " + ISSUE_TYPE);
		}

		MutableIssue newIssue=null;
		try {
			log.debug(client.toString());
			
			newIssue = issueFactory.getIssue();
			newIssue.setProjectObject(proyect);
			newIssue.setSummary(model.getSummary());
			newIssue.setDescription(model.getDescription());
			newIssue.setIssueTypeObject(itemSel);

			newIssue.setCustomFieldValue(
					customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.ASSIGNED_DEPARTMENT), optionSelected);

			newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.SCOPE),
					scopeSelected);

			newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.TARGET_DATE),
					targetDate);

			log.debug("a�adidendo cliente");
			newIssue.setCustomFieldValue(client, model.getClient());

			if (model.getFinalClient() != null && !"".equals(model.getFinalClient())) {
				newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.FINAL_CLIENT),
						model.getFinalClient());
			}

			if (model.getOfferNumber() != null && !"".equals(model.getOfferNumber())) {
				newIssue.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName(CustomFieldsValEco.OFFER_NUMBER),
						model.getOfferNumber());
			}
		} catch (Exception e) {
			log.error("",e);
		}

		return newIssue;
	}

	private Timestamp getTargetDate(String targetDate) {
		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			// you can change format of date
			Date date = formatter.parse(targetDate);
			Timestamp timeStampDate = new Timestamp(date.getTime());

			return timeStampDate;
		} catch (ParseException e) {
			log.error("getFechaObjetivo", e);
			return new Timestamp(System.currentTimeMillis());
		}

	}

	private IssueType getIssueType(String proyectName, String issueTypeName) {
		Project proyecto = ComponentAccessor.getProjectManager().getProjectObjByKey(proyectName);
		Collection<IssueType> issueTypes = ComponentAccessor.getIssueTypeSchemeManager()
				.getIssueTypesForProject(proyecto);
		IssueType itemSel = null;

		for (IssueType item : issueTypes) {
			log.debug("Issue Type:" + item.getName());

			itemSel = item;
		}

		return itemSel;

	}

	public void createAttachment(AttachmentDTO attachment, Issue issue) throws RestCrmException {

		ApplicationUser appUser = CrmRestUtils.getAppUserCrm(userJira);
		CreateAttachmentParamsBean ab = new CreateAttachmentParamsBean(attachment.getFileAttachment(),
				attachment.getFileAttachment().getName(), attachment.getContentType(), appUser, issue, false, false,
				null, new Timestamp(System.currentTimeMillis()), false);

		try {
			ComponentAccessor.getAttachmentManager().createAttachment(ab);
		} catch (AttachmentException e) {
			throw new RestCrmException("createAttachment", e);
		}

	}
}
