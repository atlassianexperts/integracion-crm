package com.atsistemas.jira.addons.ufinet.crm.rest.model.project;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ProjectsHome {

	@XmlElement(name = "site")
	private String site = null;
	@XmlElement(name = "address")
	private String address = null;
	@XmlElement(name = "latitude")
	private String latitude = null;
	@XmlElement(name = "longitude")
	private String longitude = null;
	@XmlElement(name = "region")
	private String region = null;
	@XmlElement(name = "town")
	private String town = null;
	@XmlElement(name = "country")
	private String country = null;

	/**
	 * a
	 **/
	public ProjectsHome site(String site) {
		this.site = site;
		return this;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * Dirección origen
	 **/
	public ProjectsHome address(String address) {
		this.address = address;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * latitud origen
	 **/
	public ProjectsHome latitude(String latitude) {
		this.latitude = latitude;
		return this;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * Longitud origen
	 **/
	public ProjectsHome longitude(String longitude) {
		this.longitude = longitude;
		return this;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * Provincia origen
	 **/
	public ProjectsHome region(String region) {
		this.region = region;
		return this;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Provincia origen
	 **/
	public ProjectsHome town(String town) {
		this.town = town;
		return this;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * País origen
	 **/
	public ProjectsHome country(String country) {
		this.country = country;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ProjectsHome projectsHome = (ProjectsHome) o;
		return Objects.equals(site, projectsHome.site) && Objects.equals(address, projectsHome.address)
				&& Objects.equals(latitude, projectsHome.latitude) && Objects.equals(longitude, projectsHome.longitude)
				&& Objects.equals(region, projectsHome.region) && Objects.equals(town, projectsHome.town)
				&& Objects.equals(country, projectsHome.country);
	}

	@Override
	public int hashCode() {
		return Objects.hash(site, address, latitude, longitude, region, town, country);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("    Sitio: ").append(toIndentedString(site)).append("\n");
		sb.append("    Direcci�n: ").append(toIndentedString(address)).append("\n");
		sb.append("    Latitud: ").append(toIndentedString(latitude)).append("\n");
		sb.append("    Longitud: ").append(toIndentedString(longitude)).append("\n");
		sb.append("    Region: ").append(toIndentedString(region)).append("\n");
		sb.append("    Ciudad: ").append(toIndentedString(town)).append("\n");
		sb.append("    Pa�s: ").append(toIndentedString(country)).append("\n");

		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
