package com.atsistemas.jira.addons.ufinet.crm.rest.model.project;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ProjectsClient {

	@XmlElement(name = "id")
	private String idClient = null;
	@XmlElement(name = "contactName")
	private String contactName = null;
	@XmlElement(name = "contactNumber")
	private String contactNumber = null;
	@XmlElement(name = "contactEmail")
	private String contactEmail = null;

	/**
	 * Nombre del contacto t�cnico del cliente para el servicio.
	 **/
	public ProjectsClient contactName(String contactName) {
		this.contactName = contactName;
		return this;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * Teléfono del contacto técnico del cliente para el servicio.
	 **/
	public ProjectsClient contactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
		return this;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * e-mail del contacto técnico del cliente para el servicio.
	 **/
	public ProjectsClient contactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
		return this;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ProjectsClient projectsClient = (ProjectsClient) o;
		return Objects.equals(contactName, projectsClient.contactName)
				&& Objects.equals(contactNumber, projectsClient.contactNumber)
				&& Objects.equals(contactEmail, projectsClient.contactEmail);
	}

	@Override
	public int hashCode() {
		return Objects.hash(contactName, contactNumber, contactEmail);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("    Nombre del contacto: ").append(toIndentedString(contactName)).append("\n");
		sb.append("    N�mero del contacto: ").append(toIndentedString(contactNumber)).append("\n");
		sb.append("    Email del contacto: ").append(toIndentedString(contactEmail)).append("\n");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
}
