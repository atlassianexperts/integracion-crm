package com.atsistemas.jira.addons.ufinet.crm.rest.model.project;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.LocalDate;

import com.atsistemas.jira.addons.ufinet.crm.rest.enums.AssignedDepartmentEnum;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ProjectServicesCrm {

	@XmlElement(name = "idProject")
	private String idProject = null;
	
	@XmlElement(name = "name")
	private String name = null;
	
	@XmlElement(name = "informer")
	private String informer = null;
	
	@XmlElement(name = "client")
	private String client = null;
	
	@XmlElement(name = "finalClient")
	private String finalClient = null;
	
	@XmlElement(name = "imputationCode")
	private String imputationCode = null;
	
	@XmlElement(name = "description")
	private String description = null;
	
	@XmlElement(name = "valEcoCode")
	private String valEcoCode = null;
	
	@XmlElement(name = "country")
	private String country = null;
	
	@XmlElement(name = "assignedDepartment")
	private String assignedDepartment = null;
	
	@XmlElement(name = "services")
	private List<Service> services = new ArrayList<>();

	/**
	 * id proyecto
	 **/
	public ProjectServicesCrm idProject(String idProject) {
		this.idProject = idProject;
		return this;
	}

	public String getIdProject() {
		return idProject;
	}

	public void setIdProject(String idProject) {
		this.idProject = idProject;
	}

	/**
	 * Nombre del proyecto en CRM
	 **/
	public ProjectServicesCrm name(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInformer() {
		return informer;
	}

	public void setInformer(String informer) {
		this.informer = informer;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getFinalClient() {
		return finalClient;
	}

	public void setFinalClient(String finalClient) {
		this.finalClient = finalClient;
	}

	public String getImputationCode() {
		return imputationCode;
	}

	public void setImputationCode(String imputationCode) {
		this.imputationCode = imputationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValEcoCode() {
		return valEcoCode;
	}

	public void setValEcoCode(String valEcoCode) {
		this.valEcoCode = valEcoCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public String getAssignedDepartment() {
		return assignedDepartment;
	}

	public void setAssignedDepartment(String colombia) {
		
		this.assignedDepartment = colombia;
	}
	
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ProjectServicesCrm projecto = (ProjectServicesCrm) o;
		return Objects.equals(idProject, projecto.idProject) && Objects.equals(name, projecto.name)
				&& Objects.equals(informer, projecto.informer) && Objects.equals(client, projecto.client)
				&& Objects.equals(finalClient, projecto.finalClient)
				&& Objects.equals(imputationCode, projecto.imputationCode)
				&& Objects.equals(description, projecto.description) && Objects.equals(valEcoCode, projecto.valEcoCode)
				&& Objects.equals(country, projecto.country) && Objects.equals(services, projecto.services);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idProject, name, informer, client, finalClient, imputationCode, description, valEcoCode,
				country, services);
	}



	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	@Override
	public String toString() {
		return "ProjectServicesCrm [idProject=" + idProject + ", name=" + name + ", informer=" + informer + ", client="
				+ client + ", finalClient=" + finalClient + ", imputationCode=" + imputationCode + ", description="
				+ description + ", valEcoCode=" + valEcoCode + ", country=" + country + ", assignedDepartment="
				+ assignedDepartment + ", services=" + services + "]";
	}
}
