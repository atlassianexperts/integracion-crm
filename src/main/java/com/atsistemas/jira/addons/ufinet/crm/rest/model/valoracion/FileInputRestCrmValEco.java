package com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class FileInputRestCrmValEco {
  
  @XmlElement(name = "attachment",required = true, nillable = false)
  private String attachment;
  
  @XmlElement(name = "fileName",required = true, nillable = false)
  private String fileName;
  
  @XmlElement(name = "fileExtension",required = true, nillable = false)
  private String fileExtension;
  
  @XmlElement(name = "contentType",required = true, nillable = false)
  private String contentType;
  
  @XmlElement(name = "md5")
  private String md5;

  
  public String getMd5() {
    return md5;
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  public String getAttachment() {
    return attachment;
  }

  public void setAttachment(String attachment) {
    this.attachment = attachment;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFileExtension() {
    return fileExtension;
  }

  public void setFileExtension(String fileExtension) {
    this.fileExtension = fileExtension;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

@Override
public String toString() {
	return "FileInputRestCrmValEco [attachment=" + attachment + ", fileName=" + fileName + ", fileExtension="
			+ fileExtension + ", contentType=" + contentType + ", md5=" + md5 + "]";
}

  
}
