package com.atsistemas.jira.addons.ufinet.crm.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ConfigServiceStates {
	
	public static final String CANCEL = "cancel";
	public static final String SUSPEND = "suspend";
	public static final String REACTIVATE = "reactivate";
	public static final String DELETE = "delete";
	
	public static final String TRANSITION_CANCELAR_SERVICIO="Cancelar Servicio";
	public static final int TRANSITION_ID_CANCELAR_SERVICIO=331;
	
    public static final Map<String, String> STATES_TRANSITION;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put(CANCEL, TRANSITION_CANCELAR_SERVICIO);  
        aMap.put(SUSPEND, "Pasar a Pendiente");
        aMap.put(REACTIVATE, "Reactivar");
        aMap.put(DELETE, "Rechazar Servicio");
        STATES_TRANSITION = Collections.unmodifiableMap(aMap);
    }

    public static final Map<String, String> CUSTOM_FIELD_MOTIVO;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put(CANCEL, "Motivo de Cancelación");  
        aMap.put(SUSPEND, "Motivo de Paso a Pendiente");
        aMap.put(REACTIVATE, "Motivo Reactivación");
        aMap.put(DELETE, "Motivo Reactivación"); 
        CUSTOM_FIELD_MOTIVO = Collections.unmodifiableMap(aMap);
    }
    
    public static final Map<String, String> CUSTOM_FIELD_DATE;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put(CANCEL, "Fecha Cancelación");  
        aMap.put(SUSPEND, "Fecha paso a Pendiente");
        aMap.put(REACTIVATE, "Fecha Reactivación");  
        CUSTOM_FIELD_DATE = Collections.unmodifiableMap(aMap);
    }    
    
    public static final Map<String, String> STATES_TRANSITION_REACTIVATE;
    static {
        Map<String, String> aMap = new HashMap<>();
        aMap.put("En asignación", "Reactivar");  
        aMap.put("En Planificación", "Planificar");
        aMap.put("En ejecución", "Ejecutar");  
        STATES_TRANSITION_REACTIVATE = Collections.unmodifiableMap(aMap);
    }    
    
}
