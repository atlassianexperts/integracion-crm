package com.atsistemas.jira.addons.ufinet.crm.service.utils;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.service.common.ProjectCustomFieldName;
import com.atsistemas.jira.addons.ufinet.crm.service.common.PropertiesName;
import com.atsistemas.jira.addons.ufinet.utils.UfinetUtils;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;

public class CustomFieldUtils {
	private static final Logger log = LoggerFactory.getLogger(CustomFieldUtils.class);
	
	private CustomFieldUtils() {
	}

	
	public static Map<Long, String> mapperCustomFieldIdProject(Long idProject, String issueType, I18nResolver i18nResolver,ProjectServicesCrm proyecto) {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		
		String sCustomFieldName = null;
		CustomField customSel = null;
		HashMap<Long, String> mapper = new HashMap<>();
		
		for (CustomField custom : customFieldManager.getCustomFieldObjects(idProject, issueType)) {
			try {
				
				sCustomFieldName = new String(custom.getName().getBytes("UTF-8"));
				
				if (i18nResolver.getText(ProjectCustomFieldName.ID_PROYECTO_CRM).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getIdProject());
				}
				
				if (i18nResolver.getText(ProjectCustomFieldName.NOMBRE_PROYECTO).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getName());
				}

				if (i18nResolver.getText(ProjectCustomFieldName.REPORTER).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getInformer());
				}

				if (i18nResolver.getText(ProjectCustomFieldName.CLIENTE).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getClient());
				}
				
				if (i18nResolver.getText(ProjectCustomFieldName.CLIENTE_FINAL).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getFinalClient());
				}				
				
				if (i18nResolver.getText(ProjectCustomFieldName.CODIGO_IMPUTACION).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getValEcoCode());
				}
				
				// El Area/Departamento Asignado se asigna en servicio
				
				if (i18nResolver.getText(ProjectCustomFieldName.DESCRIPCION).equals(sCustomFieldName)) {
					mapper.put(custom.getIdAsLong(), proyecto.getDescription());
				}				
				
				
			} catch (UnsupportedEncodingException e) {
				log.error("", e);

			}
			
		}
		
		return mapper;
	}	
	
	public static Map<CustomField, String> mapperCustomFieldObjectProject(Long idProject, String issueType, I18nResolver i18nResolver,ProjectServicesCrm proyecto) {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		
		String sCustomFieldName = null;
		HashMap<CustomField, String> mapper = new HashMap<>();
		
		for (CustomField custom : customFieldManager.getCustomFieldObjects(idProject, issueType)) {
			try {
				
				sCustomFieldName = new String(custom.getName().getBytes("UTF-8"));
				
				if (i18nResolver.getText(ProjectCustomFieldName.ID_PROYECTO_CRM).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getIdProject());
				}
				
				if (i18nResolver.getText(ProjectCustomFieldName.NOMBRE_PROYECTO).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getName());
				}

				if (i18nResolver.getText(ProjectCustomFieldName.REPORTER).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getInformer());
				}

				if (i18nResolver.getText(ProjectCustomFieldName.CLIENTE).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getClient());
				}
				
				if (i18nResolver.getText(ProjectCustomFieldName.CLIENTE_FINAL).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getFinalClient());
				}				
				
				if (i18nResolver.getText(ProjectCustomFieldName.CODIGO_IMPUTACION).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getValEcoCode());
				}
				
				if (i18nResolver.getText(ProjectCustomFieldName.DESCRIPCION).equals(sCustomFieldName)) {
					mapper.put(custom, proyecto.getDescription());
				}				
				

			} catch (UnsupportedEncodingException e) {
				log.error("", e);

			}
			
		}
		
		return mapper;
	}		

//	public static Map<Long, String> mapperCustomFieldServices(Long idProject, String issueType, I18nResolver i18nResolver,
//			Service servicioCrm) {
//		
//		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
//		OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
//		
//		String sCustomFieldName = null;
//		CustomField customSel = null;
//		HashMap<Long, String> mapper = new HashMap<>();
//		
//		log.debug("CUSTOM FIELD SERVICIOS");
//		for (CustomField custom : customFieldManager.getCustomFieldObjects(idProject, issueType)) {
//			
//			log.debug(custom.getId()+":"+custom.getName());
//			
//			try {
//				
//				sCustomFieldName = new String(custom.getName().getBytes("UTF-8"));
//				
//				if (i18nResolver.getText(PropertiesName.CUSTOM_FIELD_ID_SERVICIO_CRM).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getIdService());
//				}
//				
//				if (i18nResolver.getText(PropertiesName.CUSTOM_FIELD_TITULO_CORTO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getName());
//				}
//				
//				if (i18nResolver.getText(PropertiesName.CUSTOM_FIELD_CLIENTE_SERVICIO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getClientName());
//				}				
//
//				if (i18nResolver.getText("Area/Departamento  Asignado").equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getAssignedDepartment());
//				}				
//
//				
//				if (i18nResolver.getText(PropertiesName.FECHA_COMPROMISO).equals(sCustomFieldName)) {
//					log.debug(custom.getIdAsLong()+":"+sCustomFieldName+"->"+servicioCrm.getTargetDate());
//					
//					DateFormat formatterDes = new SimpleDateFormat("dd/MMM/yy");
//					DateFormat formatterOri = new SimpleDateFormat("dd/MM/yyyy");
//					
//					try {
//						Date d = formatterOri.parse(servicioCrm.getTargetDate());
//						log.debug("Fecha formateada "+formatterDes.format(d)+" fecha del dia"+formatterDes.format(new Date()));
//						mapper.put(custom.getIdAsLong(), formatterDes.format(new Date()));
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//				}
//
////				if (i18nResolver.getText(PropertiesName.TIPO_SERVICIO).equals(sCustomFieldName)) {
////
////					FieldConfig fieldConfig = custom.getRelevantConfig(new IssueContextImpl(idProject,issueType));
////					
////					List<Option> existingOptions = optionsManager.getOptions(fieldConfig);
////					
////			        for( Option option : existingOptions)
////			        {
////			        	log.info( option.getOptionId() + " ==> " + option.getValue() );
////			        	
////			        	if (option.getValue().equals(servicioCrm.getServiceType())) {
////			        		log.info("a�adiendo tipo de servicio");
////			        		mapper.put(custom.getIdAsLong(), Long.toString(option.getOptionId()));		
////			        	}
////			            
////			        }					
////					
////					
////				}
//				
//				if (i18nResolver.getText(PropertiesName.TIPO_CODIGO_ADMINISTRATIVO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getCode());
//				}				
//
//				if (i18nResolver.getText(PropertiesName.NOMBRE_CONTACTO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getClient().getContactName());
//				}				
//				
//				if (i18nResolver.getText(PropertiesName.TELEFONO_CONTACTO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getClient().getContactNumber());
//				}				
//
//				if (i18nResolver.getText(PropertiesName.EMAIL_CONTACTO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getClient().getContactEmail());
//				}				
//
//				if (i18nResolver.getText(PropertiesName.PI).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getPi());
//				}				
//				
//				
//				if (servicioCrm.getHome()!=null) {
//					
//					if (i18nResolver.getText(PropertiesName.DIRECCION_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getAddress());
//					}				
//
//					if (i18nResolver.getText(PropertiesName.SITIO_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getSite());
//					}				
//
//					if (i18nResolver.getText(PropertiesName.LATITUD_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getLatitude());
//					}				
//
//					if (i18nResolver.getText(PropertiesName.LONGITUD_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getLongitude());
//					}				
//
//					if (i18nResolver.getText(PropertiesName.PROVINCIA_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getRegion());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.MUNICIPIO_ORIGEN).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getTown());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.PAIS_ORIGEN).equals(sCustomFieldName)) {
//						//
//						mapper.put(custom.getIdAsLong(), servicioCrm.getHome().getCountry());
//					}				
//					
//				}
//				
//				if (servicioCrm.getDestination()!=null) {
//					if (i18nResolver.getText(PropertiesName.DIRECCION_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getAddress());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.SITIO_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getAddress());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.LATITUD_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getLatitude());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.LONGITUD_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getLongitude());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.PROVINCIA_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getRegion());
//					}				
//
//					if (i18nResolver.getText(PropertiesName.MUNICIPIO_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getTown());
//					}				
//					
//					if (i18nResolver.getText(PropertiesName.PAIS_DESTINO).equals(sCustomFieldName)) {
//						mapper.put(custom.getIdAsLong(), servicioCrm.getDestination().getCountry());
//					}				
//					
//				}
//
//				if (i18nResolver.getText(PropertiesName.TRANSMISION).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getTransmission());
//				}				
//				
//				if (i18nResolver.getText(PropertiesName.CAPACIDAD).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getCapability());
//				}				
//				
//				if (i18nResolver.getText(PropertiesName.FIBRAS).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getFibersNumber());
//				}				
//
//				if (i18nResolver.getText(PropertiesName.ID_VALORACION_ECO).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getIdValEco());
//				}				
//				
//				if (i18nResolver.getText(PropertiesName.PRESUPUESTO).equals(sCustomFieldName)) {
//					
//					Double lPresupuesto = new Double(servicioCrm.getServiceBudget());
//					
//					mapper.put(custom.getIdAsLong(), lPresupuesto.toString());
//				}				
//				
//				if (i18nResolver.getText(PropertiesName.OBSERVACIONES).equals(sCustomFieldName)) {
//					mapper.put(custom.getIdAsLong(), servicioCrm.getComment());
//				}				
//
//				
//			} catch (UnsupportedEncodingException e) {
//				log.error("", e);
//
//			}
//			
//		}
//		log.debug("------------");
//		return mapper;
//	}
	
	public static Map<CustomField, String> mapperCustomFieldObjectServices(Long idProject, String issueType, I18nResolver i18nResolver,
			Service servicioCrm) {
		
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
		
		String sCustomFieldName = null;
		CustomField customSel = null;
		HashMap<CustomField, String> mapper = new HashMap<>();
		
		log.debug("CUSTOM FIELD SERVICIOS");
		for (CustomField custom : customFieldManager.getCustomFieldObjects(idProject, issueType)) {
			
			log.debug(custom.getId()+":"+custom.getName());
			
			try {
				
				sCustomFieldName = new String(custom.getName().getBytes("UTF-8"));
				
				if ("ID servicio CRM".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getIdService());
				}
				
				if ("Cliente Servicio".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getClientName());
				}				

//				if (i18nResolver.getText("Area/Departamento  Asignado").equals(sCustomFieldName)) {
//					
//					mapper.put(custom, servicioCrm.getAssignedDepartment());
//				}				

				
				if (i18nResolver.getText(PropertiesName.FECHA_COMPROMISO).equals(sCustomFieldName)) {
					log.debug(custom.getIdAsLong()+":"+sCustomFieldName+"->"+servicioCrm.getTargetDate());
					
					DateFormat formatterDes = new SimpleDateFormat("dd/MMM/yy");
					DateFormat formatterOri = new SimpleDateFormat("dd/MM/yyyy");
					
					try {
						Date d = formatterOri.parse(servicioCrm.getTargetDate());
						log.debug("Fecha formateada "+formatterDes.format(d)+" fecha del dia"+formatterDes.format(new Date()));
						mapper.put(custom, formatterDes.format(new Date()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}


				
				if ("C�digo administrativo del Servicio".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getCode());
				}				

				if ("Nombre del Contacto".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getClient().getContactName());
				}				
				
				if ("Tel�fono del Contacto".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getClient().getContactNumber());
				}				

				if ("Email del Contacto".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getClient().getContactEmail());
				}				

				if ("PI".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getPi());
				}				
				
				
				if (servicioCrm.getHome()!=null) {
					
					if ("Direcci�n origen".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getAddress());
					}				

					if ("Sitio Origen".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getSite());
					}				

					if (i18nResolver.getText(PropertiesName.LATITUD_ORIGEN).equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getLatitude());
					}				

					if (i18nResolver.getText(PropertiesName.LONGITUD_ORIGEN).equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getLongitude());
					}				

					if ("Provincia origen".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getRegion());
					}				
					
					if ("Municipio origen".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getHome().getTown());
					}				
					
					if ("Pa�s origen".equals(sCustomFieldName)) {
						//
						mapper.put(custom, servicioCrm.getHome().getCountry());
					}				
					
				}
				
				if (servicioCrm.getDestination()!=null) {
					if ("Sitio Destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getSite());
					}				
					
					if ("Direcci�n destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getAddress());
					}				
					
					if ("Latitud destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getLatitude());
					}				
					
					if ("Longitud destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getLongitude());
					}				
					
					if ("Provincia destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getRegion());
					}				

					if ("Municipio destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getTown());
					}				
					
					if ("Pa�s destino".equals(sCustomFieldName)) {
						mapper.put(custom, servicioCrm.getDestination().getCountry());
					}				
					
				}

				if ("Medio de transmisi�n".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getTransmission());
				}				
				
				if ("Capacidad Servicio".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getCapability());
				}				
				
				if ("Velocidad / N� Fibras".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getFibersNumber());
				}				

				if ("ID Valoraci�n Econ�mica".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getIdValEco());
				}				
				
//				if (i18nResolver.getText(PropertiesName.PRESUPUESTO).equals(sCustomFieldName)) {
//					
//					Double lPresupuesto = new Double(servicioCrm.getServiceBudget());
//					
//					mapper.put(custom, lPresupuesto.toString());
//				}				
				
				if ("Observaciones".equals(sCustomFieldName)) {
					mapper.put(custom, servicioCrm.getComment());
				}				

				
			} catch (UnsupportedEncodingException e) {
				log.error("", e);

			}
			
		}
		log.debug("------------");
		return mapper;
	}

	
}
