package com.atsistemas.jira.addons.ufinet.crm.exception;

public class RestCrmParamException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6307662760297843484L;

	public RestCrmParamException(String message) {
		super(message);
	}

	public RestCrmParamException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
