package com.atsistemas.jira.addons.ufinet.crm.exception;

public class RestCrmException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4498439708714095030L;

	public RestCrmException(String message) {
		super(message);
	}

	public RestCrmException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
