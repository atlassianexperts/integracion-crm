package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.ufinet.crm.common.ConfigServiceStates;
import com.atsistemas.jira.addons.ufinet.crm.common.ModificationTypes;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Cause;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.ModService;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;
import com.atsistemas.jira.addons.ufinet.crm.service.SearchIssueService;
import com.atsistemas.jira.addons.ufinet.crm.service.ServiceIssueService;
import com.atsistemas.jira.addons.ufinet.crm.service.TransitionService;
import com.atsistemas.jira.addons.ufinet.crm.service.common.PropertiesName;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;

public class ServiceIssueServiceImpl implements ServiceIssueService {

	private static final String ALTA = "ALTA";
	private static final String BAJA = "BAJA";
	private static final String MODIFICACION = "MODIFICACI�N";
	
	private final I18nResolver i18nResolver;
	private final SearchIssueService searchIssueService;
	private final TransitionService transitionService;
	private final CreateServService createServService;
	private final IssueLinkManager issueLinkManager;
	private final ProjectsService projectsService;
	private final CustomFieldManager customFieldManager;

	private static final Logger logger = LoggerFactory.getLogger(ServiceIssueServiceImpl.class);

	public ServiceIssueServiceImpl(I18nResolver i18nResolver, SearchIssueService searchIssueService,
			TransitionService transitionService, CreateServService createServService, IssueLinkManager issueLinkManager,
			ProjectsService projectsService, CustomFieldManager customFieldManager) {
		super();
		this.i18nResolver = i18nResolver;
		this.searchIssueService = searchIssueService;
		this.transitionService = transitionService;
		this.createServService = createServService;
		this.issueLinkManager = issueLinkManager;
		this.projectsService = projectsService;
		this.customFieldManager = customFieldManager;
	}

	@Override
	public void updateServiceIssue(Service origin, Service update, String type) throws Exception {
		logger.trace("INICIO");
		Issue servicioOriginal = getIssue(origin.getIdService(), origin.getInformer());
		
		if (servicioOriginal == null || yaConstruido(servicioOriginal)) {  // ACTIVO
			logger.debug("servicioOriginal->"+servicioOriginal.getKey());
			if (isTipo1(type)) {
				logger.debug("-1- Modificacion del servicio "+origin.getIdService()+" tipo1 servicio ya construido");
				String tipoDeCambio = ModificationTypes.NO_CHANGE_TECNOLOGY.get(type);
				Issue modIssue = createProjectService(origin, update, MODIFICACION); // No informar a INV de la orden de modificacion

				
			} else if (isTipo2(type)) {
				String tipoDeCambio = ModificationTypes.CHANGE_TECNOLOGY.get(type);
				logger.debug("-2- Modificacion del servicio "+origin.getIdService()+" tipo2 servicio ya construido");

				Issue modIssue = createProjectServiceActivo(origin, update); // No enviar ninguna notificacion a INV en BAJA
			}
		} else {  // EN CONSTRUCCION
			logger.debug("servicioOriginal->"+servicioOriginal.getKey());
			if (isTipo1(type)) {
				String tipoDeCambio = ModificationTypes.NO_CHANGE_TECNOLOGY.get(type);
				logger.debug("-3- Modificacion del servicio "+origin.getIdService()+" del tipo1 servicio en construccion");
				updateIssue(origin,update,tipoDeCambio); // No informar a INV
				transitionService.transitionEnRevision(type, origin, update); 

			} else if (isTipo2(type)) {
				String tipoDeCambio = ModificationTypes.CHANGE_TECNOLOGY.get(type);
				
				logger.debug("-4- Modificacion del servicio "+origin.getIdService()+" del tipo2 servicio en construccion");
				Issue servicioMod = createProjectService(origin, update, MODIFICACION); // Enviar llamada a INV
				transitionService.transitionCancelarServicio(type, origin);
				linkServiceToService(servicioMod,servicioOriginal);

			}

		}

	}

	private void actualizarTipoCambio(Issue modIssue, String tipoDeCambio) {
		IssueManager issueManager = ComponentAccessor.getIssueManager();
		MutableIssue issueUpdate = issueManager.getIssueObject(modIssue.getId());
		
		CustomField cfTipoCambio = customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipo Cambio de alcance");
		Option opTipoCambio = CrmRestUtils.getOptionFormCustomField(cfTipoCambio,"Default Configuration Scheme for MAAT GSER - Tipo Cambio de alcance",tipoDeCambio);
		
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipo Cambio de alcance"),
				opTipoCambio);
		logger.trace("Modificando issue con usuario " + issueUpdate.getReporterUser().getName());
		issueManager.updateIssue(issueUpdate.getReporterUser(), issueUpdate, EventDispatchOption.DO_NOT_DISPATCH,
				false);
		
	}

	private Issue createProjectServiceActivo(Service origin, Service update) throws Exception {

		ProjectServicesCrm proyecto = new ProjectServicesCrm();
		proyecto.setIdProject(update.getIdService());
		proyecto.setAssignedDepartment(update.getAssignedDepartment());
		proyecto.setClient(update.getClientName());
		proyecto.setDescription(update.getDescription());
		proyecto.setName(ALTA + " " +update.getName()+" "+BAJA+" "+origin.getName());
		proyecto.setInformer(update.getInformer());
		

		try {
			String descripcionUpdate = update.getDescription();
			update.setDescription(descripcionUpdate+" "+getAllDescription(origin));

			Issue project = projectsService.createProject(proyecto, update.getAssignedDepartment(), ALTA+" "+BAJA);
			Issue servicioAlta = createServService.createService(update,project, ALTA);
			Issue servicioBaja = createServService.createService(update,project, BAJA); // No enviar ninguna notificacion a CRM ni INV
			projectsService.linkServiceToProject(project, servicioAlta, 0L);
			projectsService.linkServiceToProject(project, servicioBaja, 0L);
			return servicioAlta;

		} catch (Exception e) {
			throw e;

		}

	}
	private Issue createProjectService(Service origin, Service update, String tipoOperacion) throws Exception {
		ProjectServicesCrm proyecto = new ProjectServicesCrm();
		proyecto.setAssignedDepartment(update.getAssignedDepartment());
		proyecto.setClient(update.getClientName());
		proyecto.setDescription(update.getDescription());
		proyecto.setName(tipoOperacion+" "+origin.getName());
		proyecto.setInformer(update.getInformer());


		try {
			String descripcionUpdate = update.getDescription();
			update.setDescription(descripcionUpdate+" "+getAllDescription(origin));
			
			Issue project = projectsService.createProject(proyecto, update.getAssignedDepartment(), tipoOperacion);
			Issue servicio = createServService.createService(update,project,tipoOperacion);
			projectsService.linkServiceToProject(project, servicio, 0L);
			
			return servicio;

		} catch (Exception e) {
			throw e;

		}

	}

	private String getAllDescription(Service origin) {
		return "\n\n --- Servicio Original ---  \n"+origin.toStringDescription();
	}

	private boolean isTipo2(String tipo) {
		return ModificationTypes.CHANGE_TECNOLOGY.containsKey(tipo);
	}

	private boolean isTipo1(String tipo) {

		return ModificationTypes.NO_CHANGE_TECNOLOGY.containsKey(tipo);
	}

	private boolean yaConstruido(Issue servicio) {
		List<String> listado = new ArrayList();
		listado.add("En Validaci�n Servicio");
		listado.add("Servicio Aceptado");
		listado.add("Cerrada");
		listado.add("Cancelado");
		listado.add("Pendiente Cancelaci�n");
		listado.add("Baja Finalizada");
		listado.add("Baja Servicio");

		if (servicio == null) {
			return false;
		}

		String estatus = servicio.getStatusObject().getName();
		Log.debug("Estado de la issue "+servicio.getKey()+" "+estatus);
		return listado.contains(estatus);

	}

	private Issue getIssue(String idServicioCrm, String informer) throws Exception {
		List<Issue> listadoIssue = searchIssueService.searchIssueFromCustomField(
				i18nResolver.getText(PropertiesName.SERVICE_PROJECT_NAME),
				i18nResolver.getText(PropertiesName.SERVICE_ISSUETYPE_NAME),
				i18nResolver.getText(PropertiesName.CUSTOM_FIELD_ID_SERVICIO_CRM), idServicioCrm,
				CrmRestUtils.getUserCrm(informer));
		if (listadoIssue.isEmpty()) {
			return null;
		} else {
			return listadoIssue.get(0);
		}

	}

	/*
	 * Link 2 issue como relateTo
	 */
	private void linkServiceToService(Issue project, Issue service) throws CreateException {

		issueLinkManager.createIssueLink(project.getId(), service.getId(), 10003L, 0L, project.getReporterUser());

	}

	private void updateIssue(Service origin, Service update, String motivoMod) throws Exception {
		Issue servicioAModificar = getIssue(origin.getIdService(), origin.getInformer());
		IssueManager issueManager = ComponentAccessor.getIssueManager();
		MutableIssue issueUpdate = issueManager.getIssueObject(servicioAModificar.getId());

		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Capacidad Servicio"),
				update.getCapability());

		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Sitio Destino"),
				update.getDestination().getSite());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Latitud destino"),
				update.getDestination().getLatitude());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Longitud destino"),
				update.getDestination().getLongitude());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Provincia destino"),
				update.getDestination().getRegion());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Municipio destino"),
				update.getDestination().getTown());

		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Sitio Origen"),
				update.getHome().getSite());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Latitud origen"),
				update.getHome().getLatitude());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Longitud origen"),
				update.getHome().getLongitude());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Provincia origen"),
				update.getHome().getRegion());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Municipio origen"),
				update.getHome().getTown());

		logger.debug(update.getIdService() + " " + update.getDescription() + " " + update.getName());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("ID servicio CRM"),
				update.getIdService());

		Double lPresupuesto = new Double(update.getServiceBudget());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Presupuesto del Servicio"),
				lPresupuesto);
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("T�tulo Corto del Servicio"),
				update.getName());

		DateFormat formatterOri = new SimpleDateFormat("yyyy-MM-dd");
		Date d = formatterOri.parse(update.getTargetDate());

		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Fecha de Compromiso Servicio"),
				new Timestamp(d.getTime()));
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Medio de transmisi�n"),
				update.getTransmission());
		issueUpdate.setCustomFieldValue(
				customFieldManager.getCustomFieldObjectByName("C�digo administrativo del Servicio"), update.getCode());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Nombre del Contacto"),
				update.getClient().getContactName());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Tel�fono del Contacto"),
				update.getClient().getContactNumber());
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Email del Contacto"),
				update.getClient().getContactEmail());

		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Tipo Operaci�n CRM"),
				MODIFICACION);
		
		CustomField cfTipoCambio = customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipo Cambio de alcance");
		Option opTipoCambio = CrmRestUtils.getOptionFormCustomField(cfTipoCambio,"Default Configuration Scheme for MAAT GSER - Tipo Cambio de alcance",motivoMod);
		issueUpdate.setCustomFieldValue(customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipo Cambio de alcance"),
				opTipoCambio);

		issueUpdate.setDescription(update.getDescription()+getAllDescription(origin)+"----------------");
		issueUpdate.setSummary(update.getName());

		logger.trace("Modificando issue con usuario " + issueUpdate.getReporterUser().getName());
		issueManager.updateIssue(issueUpdate.getReporterUser(), issueUpdate, EventDispatchOption.DO_NOT_DISPATCH,
				false);

	}

}
