package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import java.util.Objects;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.atsistemas.jira.addons.ufinet.crm.rest.enums.AssignedDepartmentEnum;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectsClient;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectsHome;

public class Service {

	@XmlElement(name = "idService")
	private String idService = null;
	
	@XmlElement(name = "idValEco")
	private String idValEco = null;
	
	@XmlElement(name = "name")
	private String name = null;
	
	@XmlElement(name = "description")
	private String description = null;
	
	@XmlElement(name = "informer")
	private String informer = null;

	@XmlElement(name = "clientName")
	private String clientName = null;

	@XmlElement(name = "assignedDepartment")
	private String assignedDepartment = null;
	
	@XmlElement(name = "serviceType")
	private String serviceType = null;
	
	@XmlElement(name = "code")
	private String code = null;
	
	@XmlElement(name = "targetDate")
	private String targetDate = null;
	
	@XmlElement(name = "client")
	private ProjectsClient client = null;
	
	@XmlElement(name = "pi")
	private String pi = null;
	
	@XmlElement(name = "home")
	private ProjectsHome home = null;
	
	@XmlElement(name = "destination")
	private ProjectsHome destination = null;
	
	
	@XmlElement(name = "transmission")
	private String transmission = null;
	
	@XmlElement(name = "capability")
	private String capability = null;
	
	@XmlElement(name = "fibersNumber")
	private String fibersNumber = null;
	
	@XmlElement(name = "serviceBudget")
	private String serviceBudget = null;
	
	@XmlElement(name = "comment")
	private String comment = null;

	/**
	 * identificador interno del servicio
	 **/
	public Service idService(String idService) {
		this.idService = idService;
		return this;
	}

	public String getIdService() {
		return idService;
	}

	public void setIdService(String idService) {
		this.idService = idService;
	}

	/**
	 * identificador interno del servicio
	 **/
	public Service idValEco(String idValEco) {
		this.idValEco = idValEco;
		return this;
	}

	public String getIdValEco() {
		return idValEco;
	}

	public void setIdValEco(String idValEco) {
		this.idValEco = idValEco;
	}

	/**
	 * Parameter saved in the customfield \"?\"
	 **/
	public Service name(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Descripción del servicio
	 **/
	public Service description(String description) {
		this.description = description;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * KAM asociado al servicio y que lanza la orden.
	 **/
	public Service informer(String informer) {
		this.informer = informer;
		return this;
	}

	public String getInformer() {
		return informer;
	}

	public void setInformer(String informer) {
		this.informer = informer;
	}

	/**
	 * Departamento asignado
	 **/
	public Service assignedDepartment(String assignedDepartment) {
		this.assignedDepartment = assignedDepartment;
		return this;
	}

	public String getAssignedDepartment() {
		return assignedDepartment;
	}

	public void setAssignedDepartment(String assignedDepartment) {
		this.assignedDepartment = assignedDepartment;
	}

	/**
	 * aaa
	 **/
	public Service serviceType(String serviceType) {
		this.serviceType = serviceType;
		return this;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	/**
	 * Código administrativo del Servicio
	 **/
	public Service code(String code) {
		this.code = code;
		return this;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 **/
	public Service client(ProjectsClient client) {
		this.client = client;
		return this;
	}

	public ProjectsClient getClient() {
		return client;
	}

	public void setClient(ProjectsClient client) {
		this.client = client;
	}

	/**
	 * PI asociada al servicio
	 **/
	public Service pi(String pi) {
		this.pi = pi;
		return this;
	}

	public String getPi() {
		return pi;
	}

	public void setPi(String pi) {
		this.pi = pi;
	}

	/**
	 **/
	public Service home(ProjectsHome home) {
		this.home = home;
		return this;
	}

	public ProjectsHome getHome() {
		return home;
	}

	public void setHome(ProjectsHome home) {
		this.home = home;
	}

	/**
	 **/
	public Service destination(ProjectsHome destination) {
		this.destination = destination;
		return this;
	}

	public ProjectsHome getDestination() {
		return destination;
	}

	public void setDestination(ProjectsHome destination) {
		this.destination = destination;
	}


	/**
	 * Interfaz
	 **/
	public Service transmission(String transmission) {
		this.transmission = transmission;
		return this;
	}

	public String getTransmission() {
		return transmission;
	}

	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}

	/**
	 * Interfaz
	 **/
	public Service capability(String capability) {
		this.capability = capability;
		return this;
	}

	public String getCapability() {
		return capability;
	}

	public void setCapability(String capability) {
		this.capability = capability;
	}

	/**
	 * Interfaz
	 **/
	public Service fibersNumber(String fibersNumber) {
		this.fibersNumber = fibersNumber;
		return this;
	}

	public String getFibersNumber() {
		return fibersNumber;
	}

	public void setFibersNumber(String fibersNumber) {
		this.fibersNumber = fibersNumber;
	}

	/**
	 * Presupuesto del Servicio
	 **/
	public Service serviceBudget(String serviceBudget) {
		this.serviceBudget = serviceBudget;
		return this;
	}

	public String getServiceBudget() {
		return serviceBudget;
	}

	public void setServiceBudget(String serviceBudget) {
		this.serviceBudget = serviceBudget;
	}

	/**
	 * Observaciones
	 **/
	public Service comment(String comment) {
		this.comment = comment;
		return this;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Service service = (Service) o;
		return Objects.equals(idService, service.idService) && Objects.equals(idValEco, service.idValEco)
				&& Objects.equals(name, service.name) && Objects.equals(description, service.description)
				&& Objects.equals(informer, service.informer)
				&& Objects.equals(assignedDepartment, service.assignedDepartment)
				&& Objects.equals(serviceType, service.serviceType) && Objects.equals(code, service.code)
				&& Objects.equals(client, service.client) && Objects.equals(pi, service.pi)
				&& Objects.equals(home, service.home) && Objects.equals(destination, service.destination)
				&& Objects.equals(transmission, service.transmission)
				&& Objects.equals(capability, service.capability) && Objects.equals(fibersNumber, service.fibersNumber)
				&& Objects.equals(serviceBudget, service.serviceBudget) && Objects.equals(comment, service.comment);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idService, idValEco, name, description, informer, assignedDepartment, serviceType, code,
				client, pi, home, destination, transmission, capability, fibersNumber, serviceBudget,
				comment);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Service {\n");

		sb.append("    idService: ").append(toIndentedString(idService)).append("\n");
		sb.append("    idValEco: ").append(toIndentedString(idValEco)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    informer: ").append(toIndentedString(informer)).append("\n");
		sb.append("    assignedDepartment: ").append(toIndentedString(assignedDepartment)).append("\n");
		sb.append("    serviceType: ").append(toIndentedString(serviceType)).append("\n");
		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    client: ").append(toIndentedString(client)).append("\n");
		sb.append("    pi: ").append(toIndentedString(pi)).append("\n");
		sb.append("    home: ").append(toIndentedString(home)).append("\n");
		sb.append("    destination: ").append(toIndentedString(destination)).append("\n");
		sb.append("    transmission: ").append(toIndentedString(transmission)).append("\n");
		sb.append("    capability: ").append(toIndentedString(capability)).append("\n");
		sb.append("    fibersNumber: ").append(toIndentedString(fibersNumber)).append("\n");
		sb.append("    serviceBudget: ").append(toIndentedString(serviceBudget)).append("\n");
		sb.append("    comment: ").append(toIndentedString(comment)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public String toStringDescription() {
		StringBuilder sb = new StringBuilder();
		sb.append("    Valoraci�n Economica: ").append(toIndentedString(idValEco)).append("\n");
		sb.append("    Nombre: ").append(toIndentedString(name)).append("\n");
		sb.append("    Descripcion: ").append(toIndentedString(description)).append("\n");
		sb.append("    Reporter: ").append(toIndentedString(informer)).append("\n");
		sb.append("    �rea/Departamento Asignado: ").append(toIndentedString(assignedDepartment)).append("\n");
		sb.append("    Tipo de servicio: ").append(toIndentedString(serviceType)).append("\n");
		sb.append("    C�digo administrativo: ").append(toIndentedString(code)).append("\n");
		sb.append("    Cliente: ").append(toIndentedString(client)).append("\n");
		sb.append("    PI: ").append(toIndentedString(pi)).append("\n");
		sb.append("    Origen: ").append(toIndentedString(home)).append("\n");
		sb.append("    Destino: ").append(toIndentedString(destination)).append("\n");
		sb.append("    Transmision: ").append(toIndentedString(transmission)).append("\n");
		sb.append("    Capacidad: ").append(toIndentedString(capability)).append("\n");
		sb.append("    N�mero de fibras: ").append(toIndentedString(fibersNumber)).append("\n");
		sb.append("    Presupuesto: ").append(toIndentedString(serviceBudget)).append("\n");
		sb.append("    Comentarios: ").append(toIndentedString(comment)).append("\n");
		return sb.toString();
	}
	
	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
}
