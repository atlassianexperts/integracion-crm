package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
public class DetailErrorCrmValEco {

	@JsonProperty
    private String httpCode;
	
	@JsonProperty
    private String code;
	
	@JsonProperty
    private String info;

	public DetailErrorCrmValEco(String httpCode, String code, String info) {
		super();
		this.httpCode = httpCode;
		this.code = code;
		this.info = info;
	}

	public String getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(String httpCode) {
		this.httpCode = httpCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	
	
}
