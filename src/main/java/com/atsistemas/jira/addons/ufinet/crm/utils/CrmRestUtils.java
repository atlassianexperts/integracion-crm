package com.atsistemas.jira.addons.ufinet.crm.utils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
//import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.*;
import com.atlassian.jira.user.ApplicationUser;
import com.atsistemas.jira.addons.ufinet.crm.dto.AttachmentDTO;
import com.atsistemas.jira.addons.ufinet.crm.exception.RestCrmException;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.FileInputRestCrmValEco;

public class CrmRestUtils {
	private static final Logger log = LoggerFactory.getLogger(CrmRestUtils.class);

	public static AttachmentDTO getFileFromUrl(String attachment, Long idAttachment) throws RestCrmException {
		URL url;
		try {
			url = new URL(attachment);
			HttpURLConnection httpUrl = (HttpURLConnection) url.openConnection();
			String contentType = httpUrl.getContentType();

			String extension = attachment.substring(attachment.lastIndexOf("."));

			String tDir = System.getProperty("java.io.tmpdir");
			String path = tDir + idAttachment + extension;

			File fAttachment = new File(path);

			fAttachment.deleteOnExit();
			FileUtils.copyURLToFile(url, fAttachment, 1000, 1000);

			httpUrl.disconnect();

			return new AttachmentDTO(fAttachment, contentType);

		} catch (MalformedURLException e) {
			throw new RestCrmException("Create issue", e);
		} catch (IOException e) {
			throw new RestCrmException("Create issue", e);
		}

	}

	private static byte[] getContentBase64(String contentBase64) throws DecoderException {

		return (byte[]) Base64.decodeBase64(contentBase64.getBytes());

	}

	public static AttachmentDTO getFileFromBase64(FileInputRestCrmValEco file) throws RestCrmException {
		File fAttachment;
		AttachmentDTO attachment = null;
		try {
			
			String extension = file.getFileExtension();
			if (extension.indexOf('.')==-1) {
				extension="."+extension;
			}
			
			fAttachment = File.createTempFile(file.getFileName(), extension);
			fAttachment.deleteOnExit();
			FileUtils.writeByteArrayToFile(fAttachment, getContentBase64(file.getAttachment()));

			String md5File = MD5Checksum.getMD5Checksum(file.getAttachment());
			if (!md5File.equals(file.getMd5())) {
				log.error("MD5Sum incorrecto: Generado->" + md5File + " Recibido->" + file.getMd5());
				return null;
			}

			attachment = new AttachmentDTO(fAttachment, file.getContentType());
		} catch (IOException e) {
			log.error("", e);
			throw new RestCrmException("Create issue", e);
		} catch (NoSuchAlgorithmException e) {
			log.error("", e);
			throw new RestCrmException("Create issue", e);
		} catch (DecoderException e) {
			log.error("", e);
			throw new RestCrmException("Create issue", e);
		}
		return attachment;
	}

	public static CustomField getCustomfieldSelected(Long idProject, String issueType, String customFieldSel) {

		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

		String sCustomFieldName = null;
		CustomField customSel = null;

		log.debug("All custom field");
		for (CustomField custom : customFieldManager.getCustomFieldObjects(idProject, issueType)) {
			try {

				sCustomFieldName = new String(custom.getName().getBytes("UTF-8"));

				if (customFieldSel.equals(sCustomFieldName) || custom.getName().equals(customFieldSel)) {
					customSel = custom;
					return customSel;
				}
			} catch (UnsupportedEncodingException e) {
				log.error("", e);

			}
		}
		return customSel;
	}

	private static FieldConfig getFieldConfig(CustomField customField, String schemSel) {

		for (FieldConfigScheme itemFieldConfig : customField.getConfigurationSchemes()) {
			log.debug("Esquema:" + itemFieldConfig.getName());

			if (itemFieldConfig.getName().equals(schemSel)) {
				return itemFieldConfig.getOneAndOnlyConfig();
			}
		}
		return null;

	}

	public static Option getOptionFormCustomField(CustomField customField, String scheme, String optionSel) {
		String optionUtf8 = null;

		FieldConfig schemeSel = getFieldConfig(customField, scheme);

		Options options = ComponentAccessor.getOptionsManager().getOptions(schemeSel);
		Option optionFound = null;

		for (Option o : options.getRootOptions()) {
			try {

				optionUtf8 = new String(o.getValue().getBytes("UTF-8"));
				// log.debug("Option->" + optionUtf8);
				if (optionUtf8.equalsIgnoreCase(optionSel) || (o.getValue().equals(optionSel))) {
					// log.debug("Option found:" + optionUtf8 + " " +
					// optionSel);
					optionFound = o;
				}
			} catch (UnsupportedEncodingException e) {
				log.error("", e);
			}

		}
		return optionFound;
	}

	public static String getDateJira(String sDate) {
		DateFormat formatterDes = new SimpleDateFormat("dd/MMM/yy", Locale.US);
		DateFormat formatterOri = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date d = formatterOri.parse(sDate);
			log.debug(
					"Fecha formateada " + formatterDes.format(d) + " fecha del dia" + formatterDes.format(new Date()));
			return formatterDes.format(d);
		} catch (ParseException e) {
			log.error("", e);
			return null;
		}
	}

	public static String getDateCrm(String sDate) {
		DateFormat formatterOri = new SimpleDateFormat("dd/MMM/yy", Locale.US);
		DateFormat formatterDes = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date d = formatterOri.parse(sDate);
			log.debug(
					"Fecha formateada " + formatterDes.format(d) + " fecha del dia" + formatterDes.format(new Date()));
			return formatterDes.format(d);
		} catch (ParseException e) {
			log.error("", e);
			return null;
		}
	}

	public static ApplicationUser getAppUserCrm(String informer) {

		ApplicationUser appUser = ComponentAccessor.getUserManager().getUserByName(informer);

		return appUser;

	}

	public static User getUserCrm(String informer) {

		ApplicationUser appUser = getAppUserCrm(informer);

		if (appUser != null) {
			return appUser.getDirectoryUser();
		} else {
			log.error("Informador "+informer+" no encontrado");
			return null;
		}
	}

	public static Timestamp getTargetDateTimestamp(String targetDate) {
		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			// you can change format of date
			Date date = formatter.parse(targetDate);
			Timestamp timeStampDate = new Timestamp(date.getTime());

			return timeStampDate;
		} catch (ParseException e) {
			log.error("getFechaObjetivo", e);
			return new Timestamp(System.currentTimeMillis());
		}

	}

	public static String getStringTimestamp(Timestamp tsFecha) {
		String s = new SimpleDateFormat("yyyy-MM-dd").format(tsFecha);

		return s;

	}

}
