package com.atsistemas.jira.addons.ufinet.crm.dto;

import java.io.File;

public class AttachmentDTO {
  private File fileAttachment;
  private String contentType;
  
  public AttachmentDTO() {
    super();
  }
  public AttachmentDTO(File fAttachment, String contentType) {
    super();
    this.fileAttachment = fAttachment;
    this.contentType = contentType;
  }
  public File getFileAttachment() {
    return fileAttachment;
  }
  public void setFileAttachment(File fAttachment) {
    this.fileAttachment = fAttachment;
  }
  public String getContentType() {
    return contentType;
  }
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }
  
  
}
