package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.apimanager.impl.ApiManagerServiceImpl;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputCrmValoration;
import com.atsistemas.jira.addons.ufinet.crm.service.CrmConfirmService;
import com.google.gson.Gson;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CrmConfirmServiceImpl implements CrmConfirmService {
	private static final String PROPERTIE_URL = "api-manager-token.url";
	private static final Logger logger = LoggerFactory.getLogger(ApiManagerServiceImpl.class);

	private final I18nResolver i18nResolver;
	private final ApiManagerService apiManagerService;

	static {
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
	        {
	            public boolean verify(String hostname, SSLSession session)
	            {
	            	logger.error("Verificando "+hostname);
	                if (hostname.equals("apimanager.ufinet.com"))
	                    return true;
	                return false;
	            }
	        });
	}
	
	private static TrustManager[] getTrustManager() {
		return new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
		} };
	}
	
	public CrmConfirmServiceImpl(I18nResolver i18nResolver, ApiManagerService apiManagerService) {
		super();
		this.i18nResolver = i18nResolver;
		this.apiManagerService = apiManagerService;
	}

	public void confirmValoration(String id, String result) {
		try {
			String asyn = i18nResolver.getText("ufinet-jira-crm.async");

			String headerToken = apiManagerService.getTokenHeader();

			InputCrmValoration input = new InputCrmValoration(id, result);
			Gson gson = new Gson();
			String inputJson = gson.toJson(input);

			logger.debug(inputJson);
			Client client = Client.create();

			if ("0".equals(asyn)) {
				logger.info("Comunicacion sincronar confirmarcion Valoracion CRM");
				logger.debug(i18nResolver.getText("ufinet-jira-crm.url") + "valorations");
				logger.debug(inputJson);
				
				WebResource webResource = client.resource(i18nResolver.getText("ufinet-jira-crm.url") + "valorations");
				
				ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
						.header("Authorization", headerToken).put(ClientResponse.class, inputJson);
				String outPutJson = response.getEntity(String.class);
				logger.info(String.format("Response from CRM %s salida: %s", response.getStatus(), outPutJson));
				
				

			} else {
				logger.info("Comunicacion sincronar confirmarcion Valoracion CRM");
				AsyncWebResource webResource = client
						.asyncResource(i18nResolver.getText(PROPERTIE_URL) + "valorations");
				Future<ClientResponse> fResponse = webResource.type("application/json")
						.header("Authorization", headerToken).put(ClientResponse.class, inputJson);
				logger.info(String.format("Response from CRM %s", fResponse.get().getStatus()));

			}

		} catch (Exception e) {
			logger.error("error", e);
		}

	}

}
