package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ModService {
	
	@XmlElement(name = "origin")
	private Service origin = null;
	@XmlElement(name = "update")
	private Service update = null;
	
	public Service getOrigin() {
		return origin;
	}
	public void setOrigin(Service origin) {
		this.origin = origin;
	}
	public Service getUpdate() {
		return update;
	}
	public void setUpdate(Service update) {
		this.update = update;
	}
	@Override
	public String toString() {
		return "ModService [origin=" + origin + ", update=" + update + "]";
	}

	
}
