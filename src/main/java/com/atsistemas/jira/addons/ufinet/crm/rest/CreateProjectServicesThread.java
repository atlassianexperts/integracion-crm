package com.atsistemas.jira.addons.ufinet.crm.rest;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.rest.utils.GrouperUtils;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;

public class CreateProjectServicesThread implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(CreateProjectServicesThread.class);
	private ProjectServicesCrm projectServices;
	private ProjectsService createProjectService;
	private CreateServService createServService;
//	private JiraAuthenticationContext jiraAuthenticationContext;

	public CreateProjectServicesThread(ProjectServicesCrm projectServices, ProjectsService createProjectService,
			CreateServService createServService) {
		super();
		this.projectServices = projectServices;
		this.createProjectService = createProjectService;
		this.createServService = createServService;
	}

	public ProjectServicesCrm getProjectServices() {
		return projectServices;
	}

	public void setProjectServices(ProjectServicesCrm projectServices) {
		this.projectServices = projectServices;
	}

	public ProjectsService getCreateProjectService() {
		return createProjectService;
	}

	public void setCreateProjectService(ProjectsService createProjectService) {
		this.createProjectService = createProjectService;
	}

	public CreateServService getCreateServService() {
		return createServService;
	}

	public void setCreateServService(CreateServService createServService) {
		this.createServService = createServService;
	}

	
	public void run() {
		
		Issue project;
		Map<String, List<Service>> projectsService = GrouperUtils.group(projectServices.getServices());
		IssueManager issueManager = ComponentAccessor.getIssueManager();

		try {
			for (Map.Entry<String, List<Service>> item : projectsService.entrySet()) {
				project = createProjectService.createProject(projectServices, item.getKey(),"ALTA");
				log.debug("Projecto Creado: "+project.getKey());
				
				String valoracionKey = projectServices.getValEcoCode();
				log.trace("Key valoracion "+valoracionKey);
				Issue valoracion = issueManager.getIssueByCurrentKey(valoracionKey);
				if (valoracion!=null) {
					createProjectService.linkValToProject(project, valoracion, 0L);	
				}
				
				Long secuence = Long.valueOf(1);
				for (Service itemService : item.getValue()) {
					
					itemService.setName(getSummaryServicioValoracion(itemService.getName(),secuence));
					
					createProjectService.linkServiceToProject(project, createServService.createService(itemService,project,"ALTA"),
							secuence);
					secuence++;
				}

			}
			
			createProjectService.confirmCrm(projectServices.getIdProject(), "true", "projects");
			
		} catch (Exception e) {

			log.error("error creating issue", e);
			createProjectService.confirmCrm(projectServices.getIdProject(), "false","projects");

		}
//		log.debug("#### Finalizando thread:"+this.getId()+" ######");

	}

	private String getSummaryServicioValoracion(String name,Long secuence) {
		StringBuilder sb = new StringBuilder("Servicio ");
		sb.append(String.format("%03d",secuence.intValue()));
		sb.append(" | ");
		sb.append(name);
		
		return sb.toString();
	}

}
