package com.atsistemas.jira.addons.ufinet.crm.rest.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;


public class GrouperUtils {
	public static Map<String, List<Service>> group(List<Service> services) {
		HashMap<String, List<Service>> hash = new HashMap<String, List<Service>>();
		
		for (Service item : services) {
			if (hash.containsKey(item.getAssignedDepartment())) {
				hash.get(item.getAssignedDepartment()).add(item);
			}
			else {
				ArrayList<Service> servicesList = new ArrayList<>();
				servicesList.add(item);
				hash.put(item.getAssignedDepartment(), servicesList);
			}
		}
		return hash;
	}
}
