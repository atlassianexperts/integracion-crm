package com.atsistemas.jira.addons.ufinet.crm.rest;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atsistemas.jira.addons.ufinet.crm.dto.AttachmentDTO;
import com.atsistemas.jira.addons.ufinet.crm.exception.RestCrmParamException;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.DetailErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.ErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.service.CrmConfirmService;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;

/**
 * A resource of message.
 */
@Path("/valorations")
public class RestCrmValEco {

	private static final Logger log = LoggerFactory.getLogger(RestCrmValEco.class);
	
	private final CrmConfirmService crmService;
	
	public RestCrmValEco(CrmConfirmService crmService) {
		super();
		this.crmService = crmService;
	}

	@POST
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	public Response getMessage(InputRestCrmValEco input) {
		DetailErrorCrmValEco detail = null;
		
		Gson gson = new Gson();
		String inputJson = gson.toJson(input);
		log.debug("INPUT:"+inputJson);

		
		try {

			checkParamInput(input);
			
			User user = CrmRestUtils.getUserCrm(input.getInformer());
			if (user==null) {
				detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
						Response.Status.BAD_REQUEST.getReasonPhrase(), "Informador no encontrado");

				return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();				
			}

			AttachmentDTO attachmentDTO = CrmRestUtils.getFileFromBase64(input.getFile());

			if (attachmentDTO == null) {
				detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
						Response.Status.BAD_REQUEST.getReasonPhrase(), "Parametro incorrecto");

				return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();
			}
			
			
			CreateValThread hilo = new CreateValThread(input,crmService);
			hilo.start();


			return Response.status(Response.Status.ACCEPTED)
					.entity("").build();

		} catch (Exception e) {
			log.error("error creating issue", e);
			detail = new DetailErrorCrmValEco(Response.Status.INTERNAL_SERVER_ERROR.name(),
					Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.toString());
			return Response.serverError().entity(new ErrorCrmValEco(detail)).build();
		}
	}

	private void checkParamInput(InputRestCrmValEco input) throws RestCrmParamException {
		if (input.getId() == null || "".equals(input.getId())) {
			throw new RestCrmParamException("Parametro id incorrecto");
		}
		
		if (input.getInformer()==null || "".equals(input.getInformer())) {
			throw new RestCrmParamException("El campo informer es obligatorio");
		}

	}

}
