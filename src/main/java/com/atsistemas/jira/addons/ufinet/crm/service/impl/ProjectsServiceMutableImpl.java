package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.CancelProject;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;
import com.atsistemas.jira.addons.ufinet.crm.service.SearchIssueService;
import com.atsistemas.jira.addons.ufinet.crm.service.common.ProjectCustomFieldName;
import com.atsistemas.jira.addons.ufinet.crm.service.common.PropertiesName;
import com.atsistemas.jira.addons.ufinet.crm.service.model.ConfirmNewProject;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.atsistemas.jira.addons.ufinet.utils.UfinetUtils;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ProjectsServiceMutableImpl implements ProjectsService {
	private final SearchIssueService searchIssue;
	private final ProjectManager projectManager;
	private final I18nResolver i18nResolver;
	private final IssueLinkManager issueLinkManager;
	private final IssueLinkTypeManager issueLinkTypeManager;
	private final ApiManagerService apiManagerService;
	private final IssueManager issueManager;
	private final IssueFactory issueFactory;

	private static final Logger log = LoggerFactory.getLogger(ProjectsServiceMutableImpl.class);

	public ProjectsServiceMutableImpl(SearchIssueService issueService, ProjectManager projectManager,
			I18nResolver i18nResolver, IssueLinkManager issueLinkManager, IssueLinkTypeManager issueLinkTypeManager,
			ApiManagerService apiManagerService, IssueManager issueManager, IssueFactory issueFactory) {
		super();
		this.searchIssue = issueService;
		this.projectManager = projectManager;
		this.i18nResolver = i18nResolver;
		this.issueLinkManager = issueLinkManager;
		this.issueLinkTypeManager = issueLinkTypeManager;
		this.apiManagerService = apiManagerService;
		this.issueManager = issueManager;
		this.issueFactory = issueFactory;
	}

	@Override
	public Issue createProject(ProjectServicesCrm proyecto, String sAsignedDepartment,String tipoOperacion) throws CreateException {

		log.debug(String.format("Creando issue Proyecto para el departamento %s", sAsignedDepartment));

		Project project = projectManager
				.getProjectObjByKeyIgnoreCase(i18nResolver.getText(PropertiesName.PROJECT_NAME));

		IssueType proyectoIssueType = UfinetUtils.getIssueType(project,
				i18nResolver.getText(PropertiesName.PROJECT_ISSUETYPE_NAME));

		log.debug(String.format("Issue Type %s", proyectoIssueType.getName()));

		User user = UserUtils.getUser(i18nResolver.getText("ufinet-jira-crm.user"));
		if (user == null) {
			throw new CreateException(String.format("user not found %s", i18nResolver.getText("ufinet-jira-crm.user")));
		}

		try {
			MutableIssue newIssue = createMutableIssueProject(proyecto, sAsignedDepartment, tipoOperacion);

			return issueManager.createIssueObject(user, newIssue);

		} catch (Exception e) {
			log.error("-Create issue", e);
			throw new CreateException("Create issue", e);
		}

	}

	private MutableIssue createMutableIssueProject(ProjectServicesCrm proyecto, String sAsignedDepartment,String tipoOperacion)
			throws Exception {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		Project project = projectManager.getProjectObjByKey(i18nResolver.getText(PropertiesName.PROJECT_NAME));
		IssueType proyectoIssueType = UfinetUtils.getIssueType(project,
				i18nResolver.getText(PropertiesName.PROJECT_ISSUETYPE_NAME));

		MutableIssue newIssue = issueFactory.getIssue();
		newIssue.setProjectObject(project);

		newIssue.setSummary(proyecto.getName());
		newIssue.setDescription(proyecto.getDescription());
		newIssue.setIssueTypeObject(proyectoIssueType);
		newIssue.setReporter(CrmRestUtils.getUserCrm(proyecto.getInformer()));

		CustomField area = customFieldManager
				.getCustomFieldObjectByName(i18nResolver.getText(ProjectCustomFieldName.DEPARTAMENTO_ASIGNADO));

		Option opcionSel = UfinetUtils.getOptionFormCustomField(area,
				"Default Configuration Scheme for Area/Departamento Asignado", sAsignedDepartment);
		log.trace("area sel " + area);

		newIssue.setCustomFieldValue(area, opcionSel);

		CustomField codImputacion = customFieldManager.getCustomFieldObjectByName("C�digo de Imputaci�n");
		log.trace("C�digo de Imputaci�n " + codImputacion.getName());
		newIssue.setCustomFieldValue(codImputacion, proyecto.getImputationCode());

		CustomField cliente = customFieldManager.getCustomFieldObjectByName("MAAT Cliente");
		log.trace("MAAT Cliente " + cliente.getName());
		newIssue.setCustomFieldValue(cliente, proyecto.getClient());

		CustomField clienteFinal = customFieldManager.getCustomFieldObjectByName("Cliente Final");
		log.trace("Cliente Final " + clienteFinal.getName());
		newIssue.setCustomFieldValue(clienteFinal, proyecto.getFinalClient());

		CustomField nombreProyecto = customFieldManager.getCustomFieldObjectByName("Nombre del Proyecto");
		log.trace("Nombre del Proyecto " + clienteFinal.getName());
		newIssue.setCustomFieldValue(nombreProyecto, proyecto.getName());

		CustomField idProjectoCrm = customFieldManager.getCustomFieldObjectByName("ID Proyecto CRM");
		log.trace("id Projecto " + clienteFinal.getName());
		newIssue.setCustomFieldValue(idProjectoCrm, proyecto.getIdProject());
		
		CustomField cfTipoOperacionCrm = customFieldManager.getCustomFieldObjectByName("Tipo Operaci�n CRM");
		log.trace("Tipo de operacion " + tipoOperacion);
		newIssue.setCustomFieldValue(cfTipoOperacionCrm, tipoOperacion);

		return newIssue;
	}

	@Override
	public void linkServiceToProject(Issue project, Issue service, Long secuence) throws CreateException {
		IssueLinkType linkType = getIssueLinkType(i18nResolver.getText(PropertiesName.PROJECT_ISSUELINKTYPE_NAME));

		if (linkType != null) {
			issueLinkManager.createIssueLink(project.getId(), service.getId(), linkType.getId(), secuence,
					project.getReporterUser());
		} else {
			throw new CreateException("Link type not found");
		}

	}

	@Override
	public void linkValToProject(Issue project, Issue valoracion, Long secuence) throws CreateException {
		IssueLinkType linkType = getIssueLinkType("Valoraci�n");

		if (linkType != null) {
			issueLinkManager.createIssueLink(project.getId(), valoracion.getId(), linkType.getId(), secuence,
					project.getReporterUser());
		} else {
			throw new CreateException("Link type not found");
		}

	}

	private IssueLinkType getIssueLinkType(String nombreLink) {
		IssueLinkType itemSel = null;
		for (IssueLinkType item : issueLinkTypeManager
				.getIssueLinkTypesByName(nombreLink)) {
			itemSel = item;
		}
		return itemSel;
	}

	@Override
	public void confirmCrm(String idCrm, String result, String path) {
		try {

			ConfirmNewProject input = new ConfirmNewProject(idCrm, result);
			Gson gson = new Gson();
			String inputJson = gson.toJson(input);

			String asyn = i18nResolver.getText("ufinet-jira-crm.async");

			String headerToken = apiManagerService.getTokenHeader();

			log.info("Comunicacion sincronar confirmarcion Valoracion CRM");
			log.debug(headerToken);
			log.debug("--- CONFIRMACION --");
			log.debug(inputJson);
			log.debug("-----");

			Client client = Client.create();
			WebResource webResource = client.resource(i18nResolver.getText("ufinet-jira-crm.url") + path);
			// "valorations"
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("Authorization", headerToken)
					.put(ClientResponse.class, inputJson);
			log.info(String.format("Response from CRM %s", response.getStatus()));
			String outPutJson = response.getEntity(String.class);

			log.debug("--- SALIDA CRM PROYECTOS --");
			log.debug(outPutJson);
			log.debug("-----");

		} catch (Exception e) {
			log.error("error", e);
		}

	}

	@Override
	public void cancelProject(CancelProject cancelProject) {

	}

}
