package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class ErrorCrmValEco {
	
	@JsonProperty
    private DetailErrorCrmValEco result;
	
	public DetailErrorCrmValEco getResult() {
		return result;
	}

	public void setResult(DetailErrorCrmValEco result) {
		this.result = result;
	}

	public ErrorCrmValEco(DetailErrorCrmValEco result) {
		super();
		this.result = result;
	}
	
}
