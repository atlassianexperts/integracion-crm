package com.atsistemas.jira.addons.ufinet.crm.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Checksum {
  public static byte[] createChecksumFile(String filename) throws NoSuchAlgorithmException, IOException  {
    InputStream fis = new FileInputStream(filename);

    byte[] buffer = new byte[1024];
    MessageDigest complete = MessageDigest.getInstance("MD5");
    int numRead;

    do {
      numRead = fis.read(buffer);
      if (numRead > 0) {
        complete.update(buffer, 0, numRead);
      }
    } while (numRead != -1);

    fis.close();
    return complete.digest();
  }

  public static byte[] createChecksum(String content) throws NoSuchAlgorithmException, IOException  {
	    MessageDigest complete = MessageDigest.getInstance("MD5");
	    
        complete.update(content.getBytes());
        
	    return complete.digest();
	  }

  // see this How-to for a faster way to convert
  // a byte array to a HEX string
  public static String getMD5Checksum(String contentFile) throws NoSuchAlgorithmException, IOException  {
    byte[] b = createChecksum(contentFile);
    
//    return new String(b,"UTF-8");
    
    String result = "";

    for (int i = 0; i < b.length; i++) {
      result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
    }
    return result;
  }

}
