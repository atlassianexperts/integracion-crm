package com.atsistemas.jira.addons.ufinet.crm.service;

import com.atsistemas.jira.addons.ufinet.crm.rest.model.Cause;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.CancelProject;

public interface TransitionService {

	void transitionStateService(String transitionName, String idService,Cause cause, String state) throws Exception;
	
	void transitionCancelarServicio(String type,Service origin) throws Exception;
	void transitionEnRevision(String type,Service origin, Service updated) throws Exception;
	
	void transitionCancelProject(CancelProject cancelProject) throws Exception;
	
	void transitionDelService(Cause cause) throws Exception;
}
