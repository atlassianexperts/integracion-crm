package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class Cause {

	@JsonProperty
	private String cause = null;
	
	@JsonProperty
	private String date = null;
	
	@JsonProperty
	private String code = null;

	@JsonProperty
	private String informer = null;
	
	@JsonProperty("service")
	@JsonIgnore
	private Service service = null;
	
	/**
	 * motivo del cambio de estado
	 **/
	public Cause cause(String cause) {
		this.cause = cause;
		return this;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * fecha de suspension
	 **/
	public Cause date(String date) {
		this.date = date;
		return this;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * codigo administrativo
	 **/
	public Cause code(String code) {
		this.code = code;
		return this;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Cause cause = (Cause) o;
		return Objects.equals(cause, cause.cause) && Objects.equals(date, cause.date)
				&& Objects.equals(code, cause.code);
	}

	@Override
	public int hashCode() {
		return Objects.hash(cause, date, code);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Cause {\n");

		sb.append("    cause: ").append(toIndentedString(cause)).append("\n");
		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}



	public String getInformer() {
		return informer;
	}

	public void setInformer(String informer) {
		this.informer = informer;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}
