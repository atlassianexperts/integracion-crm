package com.atsistemas.jira.addons.ufinet.crm.rest.enums;

public enum AssignedDepartmentEnum {

	COLOMBIA(String.valueOf("Ingenier�a Colombia")), COSTA_RICA(String.valueOf("Ingenier�a Costa Rica")), PANAM_(
			String.valueOf("Ingenier�a Panam�")), ECUADOR(String.valueOf("Ingenier�a Ecuador")), PARAGUAY(String
					.valueOf("Ingenier�a Paraguay")), EL_SALVADOR(String.valueOf("Ingenier�a El Salvador")), HONDURAS(
							String.valueOf("Ingenier�a Honduras")), NICARAGUA(
									String.valueOf("Ingenier�a Nicaragua")), GUATEMALA(
											String.valueOf("Ingenier�a Guatemala")), CLIENTES_ESPA_A(
													String.valueOf("Ingenier�a Clientes Espa�a")), CLIENTES_LATAM(
															String.valueOf("Ingenier�a Clientes Latam"));

	private String value;

	AssignedDepartmentEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public static AssignedDepartmentEnum fromValue(String v) {
		for (AssignedDepartmentEnum b : AssignedDepartmentEnum.values()) {
			if (String.valueOf(b.value).equals(v)) {
				return b;
			}
		}
		return null;
	}
}
