package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atsistemas.jira.addons.ufinet.crm.service.SearchIssueService;
import com.atsistemas.jira.addons.ufinet.utils.UfinetUtils;

public class SearchIssueServiceImpl implements SearchIssueService {

	private final ProjectManager projectManager;
	private final SearchService searchService;

	public SearchIssueServiceImpl(ProjectManager projectManager, 
			SearchService searchService) {
		super();

		this.projectManager = projectManager;
		this.searchService = searchService;
	}

	@Override
	public List<Issue> searchIssueFromCustomField(String projectName, String issueTypeName, String customFieldName,
			String value,User user) throws Exception {

		Project project = projectManager.getProjectObjByKeyIgnoreCase(projectName);

		IssueType proyectoIssueType = UfinetUtils.getIssueType(project, issueTypeName);

		Long idCustomField = UfinetUtils
				.getCustomfieldSelected(project.getId(), proyectoIssueType.getId(), customFieldName).getIdAsLong();

		JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
		Query query = jqlClauseBuilder.project(project.getName()).and().customField(idCustomField).like(value)
				.buildQuery();

		@SuppressWarnings("rawtypes")
		PagerFilter pagerFilter = PagerFilter.getUnlimitedFilter();
		SearchResults searchResults = null;
		try {
			// Perform search results
			searchResults = searchService.search(user, query,
					pagerFilter);

		} catch (SearchException e) {
			e.printStackTrace();
		}
		// return the results
		return searchResults.getIssues();

	}

}
