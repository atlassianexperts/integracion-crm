package com.atsistemas.jira.addons.ufinet.crm.service;

import com.atsistemas.jira.addons.ufinet.crm.rest.model.ModService;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;

public interface ServiceIssueService {
	
	void updateServiceIssue(Service origin, Service update, String type) throws Exception;
//	void changeTechnology(String type,ModService modService) throws Exception;
	
}
