package com.atsistemas.jira.addons.ufinet.crm.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
public class ResultCrmValEco {
	
	@JsonProperty
    private Long idValoracion;
	
	@JsonProperty
    private String result;
	
	public ResultCrmValEco(Long idValoracion, String idOferta) {
		super();
		this.idValoracion = idValoracion;
		this.result = idOferta;
	}

	public Long getIdValoracion() {
		return idValoracion;
	}

	public void setIdValoracion(Long idValoracion) {
		this.idValoracion = idValoracion;
	}

	public String getIdOferta() {
		return result;
	}

	public void setIdOferta(String idOferta) {
		this.result = idOferta;
	}
    
    
    
    
}
