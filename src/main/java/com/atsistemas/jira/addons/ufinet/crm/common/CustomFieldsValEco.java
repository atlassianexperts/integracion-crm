package com.atsistemas.jira.addons.ufinet.crm.common;

public class CustomFieldsValEco {

  public static final String ASSIGNED_DEPARTMENT = "Departamento Asignado";
  public static final String SCOPE = "�mbito";
  public static final String CLIENT = "MAAT Cliente";
  public static final String FINAL_CLIENT = "Cliente Final";
  public static final String TARGET_DATE = "Fecha de Compromiso";
  public static final String OFFER_NUMBER = "N� Oferta";
  public static final String FIELD_SCHEME = "MAAT - Gesti�n de Valoraciones";
  public static final String FIELD_SCHEME_AMBITO = "Ambito MAAT GEstion de valoraciones";
  
}
