package com.atsistemas.jira.addons.ufinet.crm.service.common;

public class ProjectCustomFieldName {
	
	public static final String ID_PROYECTO_CRM ="proyectos-servicio-rest.proyecto.customField.idProyectoCRM";
	public static final String NOMBRE_PROYECTO ="proyectos-servicio-rest.proyecto.customField.NombreDelProyecto";
	public static final String REPORTER ="proyectos-servicio-rest.proyecto.customField.Reporter";
	public static final String CLIENTE ="proyectos-servicio-rest.proyecto.customField.Cliente";
	public static final String CLIENTE_FINAL ="proyectos-servicio-rest.proyecto.customField.ClienteFinal";
	public static final String CODIGO_IMPUTACION ="proyectos-servicio-rest.proyecto.customField.CodigoDeImputacion";
	public static final String DEPARTAMENTO_ASIGNADO ="proyectos-servicio-rest.proyecto.customField.departamentoAsignado";
	public static final String DESCRIPCION ="proyectos-servicio-rest.proyecto.customField.description";
			
			
			
}
