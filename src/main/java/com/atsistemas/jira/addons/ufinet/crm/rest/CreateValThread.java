package com.atsistemas.jira.addons.ufinet.crm.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.issue.Issue;
import com.atsistemas.jira.addons.ufinet.crm.dto.AttachmentDTO;
import com.atsistemas.jira.addons.ufinet.crm.exception.RestCrmException;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.service.CrmConfirmService;
import com.atsistemas.jira.addons.ufinet.crm.service.IssueCrmService;
import com.atsistemas.jira.addons.ufinet.crm.service.impl.IssueCrmServiceImpl;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;

public class CreateValThread extends Thread {
	
	private InputRestCrmValEco input;
	private CrmConfirmService crmService;
	
	private static final Logger logger = LoggerFactory.getLogger(CreateValThread.class);
	
	
	public CreateValThread(InputRestCrmValEco input, CrmConfirmService crmService) {
		super();
		this.input = input;
		this.crmService = crmService;
	}


	public CrmConfirmService getCrmService() {
		return crmService;
	}


	public void setCrmService(CrmConfirmService crmService) {
		this.crmService = crmService;
	}


	public InputRestCrmValEco getInput() {
		return input;
	}


	public void setInput(InputRestCrmValEco input) {
		this.input = input;
	}


	public void run() {

		AttachmentDTO attachmentDTO;
		try {
			attachmentDTO = CrmRestUtils.getFileFromBase64(input.getFile());
			IssueCrmService service = new IssueCrmServiceImpl(input);
			Issue issueCrm = service.createIssue();
			logger.debug("Issue create:" + issueCrm.getId() + " " + issueCrm.getKey());
			
			service.createAttachment(attachmentDTO, issueCrm);
			
			crmService.confirmValoration(input.getId(), issueCrm.getKey());
			

		} catch (RestCrmException e) {
			logger.error("",e);
			crmService.confirmValoration(input.getId(), null);
		}


	}	

}
