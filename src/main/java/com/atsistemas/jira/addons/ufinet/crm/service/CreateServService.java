package com.atsistemas.jira.addons.ufinet.crm.service;

import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;


public interface CreateServService {
	Issue createService(Service servicioCrm, Issue proyecto, String tipoOperacion) throws CreateException;
	
	
}
