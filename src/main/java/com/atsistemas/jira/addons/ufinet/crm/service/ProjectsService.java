package com.atsistemas.jira.addons.ufinet.crm.service;


import com.atlassian.jira.issue.Issue;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.CancelProject;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;

public interface ProjectsService {
	Issue createProject(ProjectServicesCrm proyecto, String sAsignedDepartment, String tipoOperacion) throws Exception;
	
	void linkServiceToProject(Issue project, Issue service, Long secuence) throws Exception;
	void linkValToProject(Issue project, Issue valoracion, Long secuence) throws Exception;
	
	void confirmCrm(String idCrm, String result,String path);
	
	void cancelProject(CancelProject cancelProject);
	
}
