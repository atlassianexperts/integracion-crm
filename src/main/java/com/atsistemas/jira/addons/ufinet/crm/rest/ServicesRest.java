package com.atsistemas.jira.addons.ufinet.crm.rest;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atsistemas.jira.addons.ufinet.crm.common.ConfigServiceStates;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Cause;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.DetailErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.ErrorCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.ModService;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.ProjectServicesCrm;
import com.atsistemas.jira.addons.ufinet.crm.service.CreateServService;
import com.atsistemas.jira.addons.ufinet.crm.service.ProjectsService;
import com.atsistemas.jira.addons.ufinet.crm.service.ServiceIssueService;
import com.atsistemas.jira.addons.ufinet.crm.service.TransitionService;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.google.gson.Gson;

/**
 * A resource of message.
 */
@Path("/services")
public class ServicesRest {
	private static final Logger logger = LoggerFactory.getLogger(ServicesRest.class);
        		
	private final TransitionService transitionService;
	private final ServiceIssueService serviceIssueService;
	private final CreateServService createServService;
	private final ProjectsService createProjectService;
	private final ProjectsService projectsService;

	public ServicesRest(TransitionService transitionService, ServiceIssueService serviceIssueService,
			CreateServService createServService, ProjectsService createProjectService,
			ProjectsService projectsService) {
		super();
		this.transitionService = transitionService;
		this.serviceIssueService = serviceIssueService;
		this.createServService = createServService;
		this.createProjectService = createProjectService;
		this.projectsService = projectsService;
	}


	@POST
    @Path("/{idService}/{states}")
    @Consumes({ APPLICATION_JSON, APPLICATION_FORM_URLENCODED })
    @Produces({ APPLICATION_JSON })
    public Response servicesIdServiceStatesPost(@PathParam("idService") String idService,@PathParam("states") String states,Cause cause) {
    	
		Gson gson = new Gson();
		String inputJson = gson.toJson(cause);
		logger.debug("INPUT:"+inputJson);
		
		if (!ConfigServiceStates.STATES_TRANSITION.containsKey(states)) {
			
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
					Response.Status.BAD_REQUEST.getReasonPhrase(), "Parametro incorrecto");
			
			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();
		}
		
		User user = CrmRestUtils.getUserCrm(cause.getInformer());
		if (user==null) {
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
					Response.Status.BAD_REQUEST.getReasonPhrase(), "Informador no encontrado");

			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();				
		}		
		
		if (ConfigServiceStates.DELETE.equals(states)) {
			// Baja de un servicio
			return servicesDelPostService(cause);
			
		}
		else {
			String transition = ConfigServiceStates.STATES_TRANSITION.get(states);
			logger.debug(String.format("Lanzando la transicion %s", transition));
			
			try {
				transitionService.transitionStateService(transition, idService,cause,states);
			} catch (Exception e) {
				logger.error("",e);
				DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.INTERNAL_SERVER_ERROR.name(),
						Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.toString());
				return Response.serverError().entity(new ErrorCrmValEco(detail)).build();
			}
			
		}
		
		return Response.ok().entity("Solicitud aceptada.").build();
    }


//    @PUT
//    @Path("/")
//    @Consumes(APPLICATION_JSON)
//    @Produces(APPLICATION_JSON)
    public Response servicesDelPostService(Cause baja) {
    	
		User user = CrmRestUtils.getUserCrm(baja.getService().getInformer());
		logger.error("@@INFORMADOR->"+baja.getService().getInformer());
		if (user==null) {
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
					Response.Status.BAD_REQUEST.getReasonPhrase(), "Informador no encontrado");

			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();				
		}		

		
    	ProjectServicesCrm proyecto = new ProjectServicesCrm();
    	proyecto.setAssignedDepartment(baja.getService().getAssignedDepartment());
    	proyecto.setClient(baja.getService().getClientName());
    	proyecto.setDescription(baja.getService().getDescription()+" -causa de la baja-"+baja.getCause());
    	proyecto.setName(baja.getService().getName());
    	proyecto.setInformer(baja.getService().getInformer());
    	
    	try {
    		//transitionService.transitionDelService(baja);
    		
    		Issue project = createProjectService.createProject(proyecto, baja.getService().getAssignedDepartment(),"BAJA");
    		Issue servicioDeBaja = createServService.createService(baja.getService(),project,"BAJA");
    		projectsService.linkServiceToProject(project, servicioDeBaja, 0L);
    		
			
		} catch (Exception e) {
			logger.error("Error orden de baja servicio", e);
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco("500",
					Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.toString());
			return Response.serverError().entity(new ErrorCrmValEco(detail)).build();
			
		}
    	
    	return Response.ok().entity("Ok.").build();
    }

    
	@PUT
    @Path("/{type}")
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response modificarServicio(@PathParam("type") String type,ModService modService) {
    	logger.debug("####INPUT MOD SERVICE: "+modService.toString());
    	
		User user = CrmRestUtils.getUserCrm(modService.getOrigin().getInformer());
		if (user==null) {
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco(Response.Status.BAD_REQUEST.name(),
					Response.Status.BAD_REQUEST.getReasonPhrase(), "Informador no encontrado");

			return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorCrmValEco(detail)).build();				
		}
		
		
		try {
			
			serviceIssueService.updateServiceIssue(modService.getOrigin(), modService.getUpdate(), type);
			
		} catch (Exception e) {
			logger.error("",e);
			DetailErrorCrmValEco detail = new DetailErrorCrmValEco("500",
					Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.toString());
			return Response.serverError().entity(new ErrorCrmValEco(detail)).build();
		}

		
		return Response.ok().entity("Solicitud aceptada.").build();
    }
    
}