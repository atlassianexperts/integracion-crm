package com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class InputRestCrmValEco {

  @XmlElement(name = "id",required = true, nillable = false)
  private String id;
  
  @XmlElement(name = "informer",required = false, nillable = true)
  private String informer;

  @XmlElement(name = "nameApplication",required = true, nillable = false)
  private String nameApplication;

  @XmlElement(name = "assignedDepartment",required = true, nillable = false)
  private String assignedDepartment;

  @XmlElement(name = "scope",required = true, nillable = false)
  private String scope;

  @XmlElement(name = "summary",required = true, nillable = false)
  private String summary;

  @XmlElement(name = "description",required = true, nillable = false)
  private String description;

  @XmlElement(name = "client",required = true, nillable = false)
  private String client;

  @XmlElement(name = "finalClient")
  private String finalClient;

  @XmlElement(name = "targetDate",required = true, nillable = false)
  private String targetDate;

  @XmlElement(name = "offerNumber")
  private String offerNumber;

  @XmlElement(name = "file",required = true, nillable = false)
  private FileInputRestCrmValEco file;

  public InputRestCrmValEco() {}



  public InputRestCrmValEco(String id, String informer, String nameApplication, String assignedDepartment, String scope,
		String summary, String description, String client, String finalClient, String targetDate, String offerNumber,
		FileInputRestCrmValEco file) {
	super();
	this.id = id;
	this.informer = informer;
	this.nameApplication = nameApplication;
	this.assignedDepartment = assignedDepartment;
	this.scope = scope;
	this.summary = summary;
	this.description = description;
	this.client = client;
	this.finalClient = finalClient;
	this.targetDate = targetDate;
	this.offerNumber = offerNumber;
	this.file = file;
}



public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNameApplication() {
    return nameApplication;
  }

  public void setNameApplication(String nameApplication) {
    this.nameApplication = nameApplication;
  }

  public String getAssignedDepartment() {
    return assignedDepartment;
  }

  public void setAssignedDepartment(String assignedDepartment) {
    this.assignedDepartment = assignedDepartment;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getFinalClient() {
    return finalClient;
  }

  public void setFinalClient(String finalClient) {
    this.finalClient = finalClient;
  }

  public String getTargetDate() {
    return targetDate;
  }

  public void setTargetDate(String targetDate) {
    this.targetDate = targetDate;
  }

  public String getOfferNumber() {
    return offerNumber;
  }

  public void setOfferNumber(String offerNumber) {
    this.offerNumber = offerNumber;
  }

  public FileInputRestCrmValEco getFile() {
    return file;
  }

  public void setFile(FileInputRestCrmValEco file) {
    this.file = file;
  }

@Override
public String toString() {
	return "InputRestCrmValEco [id=" + id + ", nameApplication=" + nameApplication + ", assignedDepartment="
			+ assignedDepartment + ", scope=" + scope + ", summary=" + summary + ", description=" + description
			+ ", client=" + client + ", finalClient=" + finalClient + ", targetDate=" + targetDate + ", offerNumber="
			+ offerNumber + ", file=" + file + "]";
}



public String getInformer() {
	return informer;
}



public void setInformer(String informer) {
	this.informer = informer;
}

  
}
