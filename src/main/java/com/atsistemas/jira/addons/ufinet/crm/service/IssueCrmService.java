package com.atsistemas.jira.addons.ufinet.crm.service;

import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.web.util.AttachmentException;
import com.atsistemas.jira.addons.ufinet.crm.dto.AttachmentDTO;
import com.atsistemas.jira.addons.ufinet.crm.exception.RestCrmException;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;

public interface IssueCrmService {

  public Issue createIssue(String projectName, String issueTypeName, InputRestCrmValEco model) throws RestCrmException;
  
  public Issue createIssue() throws RestCrmException;
  
  public void createAttachment(AttachmentDTO attachmentDTO, Issue issue) throws RestCrmException;
}
