package com.atsistemas.jira.addons.ufinet.crm.service;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;

public interface SearchIssueService {

	List<Issue> searchIssueFromCustomField(String projectName, String issueTypeName, String customFieldName, String value, User user) throws Exception;
}
