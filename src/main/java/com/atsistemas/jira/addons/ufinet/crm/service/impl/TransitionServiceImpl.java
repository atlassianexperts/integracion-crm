package com.atsistemas.jira.addons.ufinet.crm.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.atlassian.jira.workflow.TransitionOptions;
import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.ufinet.crm.common.ConfigServiceStates;
import com.atsistemas.jira.addons.ufinet.crm.common.ModificationTypes;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Cause;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.Service;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.project.CancelProject;
import com.atsistemas.jira.addons.ufinet.crm.service.SearchIssueService;
import com.atsistemas.jira.addons.ufinet.crm.service.TransitionService;
import com.atsistemas.jira.addons.ufinet.crm.service.common.PropertiesName;
import com.atsistemas.jira.addons.ufinet.crm.utils.CrmRestUtils;
import com.opensymphony.workflow.loader.ActionDescriptor;

public class TransitionServiceImpl implements TransitionService {
	private static final Logger logger = LoggerFactory.getLogger(TransitionServiceImpl.class);

	private final IssueService issueService;
	private final SearchIssueService searchIssueService;
	private final I18nResolver i18nResolver;
	private final IssueWorkflowManager issueWorkflowManager;

	public TransitionServiceImpl(IssueService issueService, SearchIssueService searchIssueService,
			I18nResolver i18nResolver, IssueWorkflowManager issueWorkflowManager) {
		super();
		this.issueService = issueService;
		this.searchIssueService = searchIssueService;
		this.i18nResolver = i18nResolver;
		this.issueWorkflowManager = issueWorkflowManager;
	}


	private Issue getIssue(String idService, String informer) throws Exception {
		
		logger.debug("Buscando la issue %s con el usuario %s",idService,informer);
		List<Issue> listadoIssue = searchIssueService.searchIssueFromCustomField(
				i18nResolver.getText(PropertiesName.SERVICE_PROJECT_NAME),
				i18nResolver.getText(PropertiesName.SERVICE_ISSUETYPE_NAME),
				i18nResolver.getText(PropertiesName.CUSTOM_FIELD_ID_SERVICIO_CRM), idService,CrmRestUtils.getUserCrm(informer));

		for (Issue item : listadoIssue) {
			logger.debug("issue: " + item.getId() + " " + item.getKey());
		}

		return listadoIssue.get(0);

	}
	
	
	public void transitionDelService(Cause cause) throws Exception {
		Issue issueSel = getIssue(cause.getService().getIdService(), cause.getInformer());
		
		int actionId = getTransitionId(issueSel, "Rechazar Servicio");
		
		IssueInputParameters parametros = issueService.newIssueInputParameters();
		executeTransition(issueSel, parametros, actionId);
		
	}

	@Override
	public void transitionStateService(String transitionName, String idService, Cause cause,String state) throws Exception {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		
		Issue issueSel = getIssue(idService,cause.getInformer());
		
		int actionId = getTransitionId(issueSel, transitionName);
		
		if (ConfigServiceStates.REACTIVATE.equals(state)) {
			
			String valor = getLastStatus(issueSel); 
					//(String) issueSel.getCustomFieldValue(estadoPrevio);
			String newTransitionName = ConfigServiceStates.STATES_TRANSITION_REACTIVATE.get(valor);
			
			actionId = getTransitionId(issueSel, newTransitionName);
			
		}

		logger.debug("transitando a " + actionId);

		IssueInputParameters parametros = issueService.newIssueInputParameters();

		
		CustomField motivo = customFieldManager.getCustomFieldObjectByName(ConfigServiceStates.CUSTOM_FIELD_MOTIVO.get(state));
		CustomField fecha = customFieldManager.getCustomFieldObjectByName(ConfigServiceStates.CUSTOM_FIELD_DATE.get(state));
		
		
		parametros.addCustomFieldValue(motivo.getIdAsLong(), cause.getCause());
		parametros.addCustomFieldValue(fecha.getIdAsLong(), CrmRestUtils.getDateJira(cause.getDate()));
		

		executeTransition(issueSel, parametros, actionId);

	}


	public void transitionCancelarServicio(String type, Service origin) throws Exception {

		Issue issueSel = getIssue(origin.getIdService(),origin.getInformer());
		int actionId = getTransitionId(issueSel, "Cancelar Servicio");
		
		IssueInputParameters parametros = issueService.newIssueInputParameters();
		executeTransition(issueSel, parametros, actionId);		
	}
	
	
	public void transitionEnRevision(String type, Service origin, Service updated) throws Exception {
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		String tipoCambio = "";

		Issue issueSel = getIssue(origin.getIdService(),origin.getInformer());
		tipoCambio = ModificationTypes.NO_CHANGE_TECNOLOGY.get(type);
		int actionId = getTransitionId(issueSel, "Modificar Servicio");
		IssueInputParameters parametros = issueService.newIssueInputParameters();

		CustomField cfMotivo = customFieldManager.getCustomFieldObjectByName("MAAT Motivo");
		
//		CustomField cfTipoCambio = customFieldManager.getCustomFieldObjectByName("MAAT GSER - Tipo Cambio de alcance");
		
//		Option opTipoCambio = CrmRestUtils.getOptionFormCustomField(cfTipoCambio,"Default Configuration Scheme for MAAT GSER - Tipo Cambio de alcance",tipoCambio);
		
		
//		logger.trace("Opcion seleccionada "+opTipoCambio.getValue());
		
		parametros.addCustomFieldValue(cfMotivo.getIdAsLong(), tipoCambio); 

		//		parametros.addCustomFieldValue(cfTipoCambio.getIdAsLong(), opTipoCambio.getOptionId().toString());

		executeTransition(issueSel, parametros, actionId);
			
	}
	

	private int getTransitionId(Issue issue, String transitionName) {
		logger.debug("Buscando "+transitionName);
		List<ActionDescriptor> actions = issueWorkflowManager.getSortedAvailableActions(issue,
				TransitionOptions.defaults(), CrmRestUtils.getAppUserCrm(issue.getReporterUser().getName())); 
		for (ActionDescriptor actionDescriptor : actions) {
			logger.debug(actionDescriptor.getId() + " -> " + actionDescriptor.getName());

			if (transitionName.equals(actionDescriptor.getName())) {
				return actionDescriptor.getId();
			}

		}
		return 0;
	}

	private void executeTransition(Issue issueSel, IssueInputParameters parametros, int actionId) throws Exception {

		TransitionValidationResult result = issueService.validateTransition(
				CrmRestUtils.getUserCrm(issueSel.getReporter().getName()), issueSel.getId(), actionId, parametros);

		logger.debug("validacion:" + result.isValid());

		if (result.isValid()) {
			ServiceResult issueResult = issueService.transition(CrmRestUtils.getUserCrm(issueSel.getReporter().getName()),
					result);
			if (!issueResult.isValid()) {

				logger.error("Error al lanzar la transicion:" + issueResult.getErrorCollection());
				throw new Exception("Error en transicion ");
			}
		} else {
			logger.error("Validacion de la transicion incorrecta " + result.getErrorCollection());
			throw new Exception("Error en transicion ");
		}

	}

	@Override
	public void transitionCancelProject(CancelProject cancelProject) throws Exception {
		String idProjecto = cancelProject.getIdProyecto();
		CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
		
		
		Issue projecto = searchIssueService.searchIssueFromCustomField(i18nResolver.getText(PropertiesName.PROJECT_NAME),
				i18nResolver.getText(PropertiesName.PROJECT_ISSUETYPE_NAME),
				i18nResolver.getText(PropertiesName.PROJECT_CUSTOMFIELD_NAME), idProjecto,CrmRestUtils.getUserCrm("crm")).get(0);
		
		logger.debug(projecto.getKey());
		int actionId = getTransitionId(projecto, "Cancelar Proyecto");
		
		IssueInputParameters parametros = issueService.newIssueInputParameters();
		CustomField motivo = customFieldManager.getCustomFieldObjectByName("Motivo de Cancelación");
		CustomField fecha = customFieldManager.getCustomFieldObjectByName("Fecha Cancelación");
		
		parametros.addCustomFieldValue(motivo.getIdAsLong(), cancelProject.getMotivo());
		
		DateFormat formatterOri = new SimpleDateFormat("yyyy-MM-dd");
		String sFecha = formatterOri.format(new Date());

		
		parametros.addCustomFieldValue(fecha.getIdAsLong(), CrmRestUtils.getDateJira(sFecha));

		executeTransition(projecto, parametros, actionId);
		
	}
	
	private String getLastStatus(Issue issue) {
		ChangeItemBean lastStatus = null;
		
	    for(ChangeHistory changeHistory: ComponentAccessor.getChangeHistoryManager().getChangeHistories(issue)) {
	        for(ChangeItemBean changeItemBean: changeHistory.getChangeItemBeans()) {
	            if (changeItemBean.getField().equals("status")) {
	                if(lastStatus==null){
	                    lastStatus = changeItemBean;
	                }
	                else if(changeItemBean.getCreated().getTime() > lastStatus.getCreated().getTime()){
	                    lastStatus = changeItemBean;
	                }
	            }
	        }
	    }
	    if(lastStatus!=null) { 
	    	return lastStatus.getFromString();
	    }
	    else {
	    	return "En ejecución"; 
	    }
	}
	
	
}
