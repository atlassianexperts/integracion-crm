package com.atsistemas.jira.addons.api;

public interface MyPluginComponent
{
    String getName();
}