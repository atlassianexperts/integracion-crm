package ut.com.atsistemas.jira.addons.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.atsistemas.jira.addons.jira.webwork.FormCrmAction;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class formCrmActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //formCrmAction testClass = new formCrmAction();
        String url = "http://localhost:8080/secure/admin/formCrmAction!SendRest.jspa";
        String uri = "/secure/admin/formCrmAction!SendRest.jspa";
        String requestPath = url.substring(0, url.indexOf(uri));
        
        throw new Exception("formCrmAction has no tests!");

    }

}
