package ut.com.atsistemas.jira.addons;

import org.junit.Test;
import com.atsistemas.jira.addons.api.MyPluginComponent;
import com.atsistemas.jira.addons.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
        
		DateFormat formatterDes = new SimpleDateFormat("dd/MMM/yy");
		DateFormat formatterOri = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			Date d = formatterOri.parse("01/01/2018");
			System.out.println("Fecha formateada "+formatterDes.format(d)+" fecha del dia"+formatterDes.format(new Date()));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
}