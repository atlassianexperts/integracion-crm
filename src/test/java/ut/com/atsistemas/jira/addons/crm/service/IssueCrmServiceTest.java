package ut.com.atsistemas.jira.addons.crm.service;

import java.io.File;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.FileInputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.service.IssueCrmService;
import com.atsistemas.jira.addons.ufinet.crm.service.impl.IssueCrmServiceImpl;

public class IssueCrmServiceTest {
  InputRestCrmValEco input;
  
  @Before
  public void setUp() {
    input = new InputRestCrmValEco("id", "nameApplication", "assignedDepartment",
        "scope", "summary", "description", "client", "finalClient",
        "targetDate", "offerNumber", new FileInputRestCrmValEco());
  }
  
  
  @Test
  public void test1() {
    String sUrl = "http://localhost:8080/s/en_UK-rf9e1l/64027/4/_/images/icon-jira-logo.png";
    
    HttpURLConnection httpUrl=null;
    URL url;
    try {
      
      url = new URL(sUrl);

      httpUrl = (HttpURLConnection) url.openConnection();
      String contentType = httpUrl.getContentType();
      String extension = sUrl.substring(sUrl.indexOf("."));
      
      String tDir = System.getProperty("java.io.tmpdir"); 
      String path = tDir + "tmp" + extension; 
      File fAttachment = new File(path); 
      fAttachment.deleteOnExit(); 
      FileUtils.copyURLToFile(url, fAttachment);
      httpUrl.disconnect();
      
      
      Assert.assertNotNull(fAttachment);
      Assert.assertNotNull(fAttachment.getName());
      
      
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      Assert.fail(e.toString());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    finally {
      if (httpUrl!=null) {
        httpUrl.disconnect();
      }      
    }   
    
  }
  
 
}
