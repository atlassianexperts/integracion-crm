package ut.com.atsistemas.jira.addons.apimanager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.atlassian.sal.api.message.I18nResolver;
import com.atsistemas.jira.addons.apimanager.ApiManagerService;
import com.atsistemas.jira.addons.apimanager.common.ApiManagerEnum;
import com.atsistemas.jira.addons.apimanager.impl.ApiManagerServiceImpl;
import com.atsistemas.jira.addons.ufinet.crm.service.CrmConfirmService;
import com.atsistemas.jira.addons.ufinet.crm.service.impl.CrmConfirmServiceImpl;

import org.junit.runners.MethodSorters;

import org.junit.FixMethodOrder;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApiManagerServiceTest {
	private String tokenHeader;
	
	@Mock
	private I18nResolver i18nResolver;
	
	private ApiManagerService apiManagerService;
	
	@Before
	public void setUp() {
		i18nResolver = Mockito.mock(I18nResolver.class);
		Mockito.when(i18nResolver.getText(ApiManagerEnum.URL.value())).thenReturn("https://77.72.108.78:8243/token");
		Mockito.when(i18nResolver.getText(ApiManagerEnum.USER.value())).thenReturn("3jfBr6JQuayMhO60lEm4Qlr_Vk0a");
		Mockito.when(i18nResolver.getText(ApiManagerEnum.PASS.value())).thenReturn("tG0XwcabHwbih9FUf4_UbN4HpHga");
		Mockito.when(i18nResolver.getText("ufinet-jira-crm.url")).thenReturn("https://77.72.108.78:8243/crm/1.0.0/");
		Mockito.when(i18nResolver.getText("ufinet-jira-crm.async")).thenReturn("0");
		apiManagerService = new ApiManagerServiceImpl(i18nResolver);
	}
	 
	@Test
	public void test1() {
		
		tokenHeader = apiManagerService.getTokenHeader();
		System.out.println("Token "+tokenHeader);
		
		Assert.assertNotNull(tokenHeader);
	}
	@Test
	public void test2() {
		
		CrmConfirmService service = new CrmConfirmServiceImpl(i18nResolver,apiManagerService);
		
		service.confirmValoration("id", "ok");
		
		
	}
}
