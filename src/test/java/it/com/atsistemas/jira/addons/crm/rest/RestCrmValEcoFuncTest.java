package it.com.atsistemas.jira.addons.crm.rest;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.atsistemas.jira.addons.ufinet.crm.rest.RestCrmValEco;
import com.atsistemas.jira.addons.ufinet.crm.rest.model.valoracion.InputRestCrmValEco;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

public class RestCrmValEcoFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {

        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/rest/valorations/1.0/message";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        InputRestCrmValEco message = resource.get(InputRestCrmValEco.class);
        
        assertNotNull(message.toString());
        //assertEquals("wrong message","Hello World",message.getMessage());
    }
}
